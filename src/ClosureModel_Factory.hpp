// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef CLOSURE_MODEL_FACTORY_HPP
#define CLOSURE_MODEL_FACTORY_HPP

#include "Panzer_ClosureModel_Factory.hpp"

namespace panzer {
  class InputEquationSet;

  template <typename> class LinearObjFactory;
}

namespace SiYuan {

  template<typename EvalT>
  class ClosureModelFactory : public panzer::ClosureModelFactory<EvalT> {
    Teuchos::RCP<const panzer::LinearObjFactory<panzer::Traits> > distr_param_lof;

  public:
    void setDistributedParameterLOF(const Teuchos::RCP<const panzer::LinearObjFactory<panzer::Traits> > & dpl)
    { distr_param_lof = dpl; }

    std::vector< Teuchos::RCP<PHX::Evaluator<panzer::Traits> > >
    buildClosureModels(const std::string& model_id,
		       const Teuchos::ParameterList& models,
		       const panzer::FieldLayoutLibrary& fl,
		       const Teuchos::RCP<panzer::IntegrationRule>& ir,
		       const Teuchos::ParameterList& default_params,
		       const Teuchos::ParameterList& user_data,
		       const Teuchos::RCP<panzer::GlobalData>& global_data,
		       PHX::FieldManager<panzer::Traits>& fm) const;

  };

}

#include "ClosureModel_Factory_impl.hpp"

#endif
