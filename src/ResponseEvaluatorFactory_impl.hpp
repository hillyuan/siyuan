// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef __ResponseEvaluatorFactory_impl_hpp__
#define __ResponseEvaluatorFactory_impl_hpp__

#include "Panzer_Normals.hpp"
#include "Panzer_Sum.hpp"
#include "Panzer_DotProduct.hpp"
#include "Panzer_Integrator_Scalar.hpp"
#include "Panzer_SubcellSum.hpp"

namespace SiYuan {

template <typename EvalT,typename LO,typename GO>
void ResponseEvaluatorFactory<EvalT,LO,GO>::
buildAndRegisterEvaluators(const std::string & responseName,
                           PHX::FieldManager<panzer::Traits> & fm,
                           const panzer::PhysicsBlock & physicsBlock,
                           const Teuchos::ParameterList & user_data) const
{
  // create basis 
  Teuchos::RCP<const panzer::FieldLibrary> fieldLib = physicsBlock.getFieldLibrary();
  Teuchos::RCP<const panzer::PureBasis> basis = fieldLib->lookupBasis(m_dof_name);

  {
    Teuchos::ParameterList pl;
    pl.set("Sum Name",responseName);
    pl.set("Field Name","RESIDUAL_"+m_dof_name);
    pl.set("Basis",basis);
    pl.set("Multiplier",1.0);
    
    Teuchos::RCP<PHX::Evaluator<panzer::Traits> > eval 
        = Teuchos::rcp(new panzer::SubcellSum<EvalT,panzer::Traits>(pl));
 
    this->template registerEvaluator<EvalT>(fm, eval);
  }

  panzer::ResponseEvaluatorFactory_Functional<EvalT,LO,GO>::buildAndRegisterEvaluators(responseName,fm,physicsBlock,user_data);
}

}

#endif
