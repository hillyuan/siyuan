// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef __PHYSICS_BLOCK_HPP__
#define __PHYSICS_BLOCK_HPP__

#include "Panzer_PhysicsBlock.hpp"
#include "MaterialBase.hpp"

namespace SiYuan {

/** This class defines a response based on a functional. */
  
class PhysicsBlock : public panzer::PhysicsBlock
{
public:
    /** This constructor call initialize.
      */
    PhysicsBlock(const Teuchos::RCP<Teuchos::ParameterList>& physics_block_plist,
                 const std::string & element_block_id,
                 const std::string & material_id,
                 const int default_integration_order,
                 const panzer::CellData & cell_data,
                 const Teuchos::RCP<const panzer::EquationSetFactory>& factory,
                 const Teuchos::RCP<panzer::GlobalData>& global_data,
                 const bool build_transient_support,
                 const std::vector<std::string>& tangent_param_names = std::vector<std::string>());

    //void buildAndRegisterEquationSetEvaluators(PHX::FieldManager<panzer::Traits>& fm,
    //                                          const Teuchos::ParameterList& user_data) const;

    void buildAndRegisterClosureModelEvaluators(PHX::FieldManager<panzer::Traits>& fm,
                                                const panzer::ClosureModelFactory_TemplateManager<panzer::Traits>& factory,
                                                const Teuchos::ParameterList& models,
                                                const Teuchos::ParameterList& user_data) const;

    void buildAndRegisterClosureModelEvaluators(PHX::FieldManager<panzer::Traits>& fm,
                                            const panzer::ClosureModelFactory_TemplateManager<panzer::Traits>& factory,
                                            const std::string& model_name,
                                            const Teuchos::ParameterList& models,
                                            const Teuchos::ParameterList& user_data) const;

    void setMaterial(PHX::FieldManager<panzer::Traits>&, CMaterials&);

private:
  std::string _material_name;
};

}

#endif
