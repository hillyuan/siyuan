// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _Schrodinger_EquationSet_hpp_
#define _Schrodinger_EquationSet_hpp_

#include <string>

#include "Teuchos_RCP.hpp"
#include "Panzer_EquationSet_DefaultImpl.hpp"
#include "Panzer_Traits.hpp"
#include "Phalanx_FieldManager.hpp"

namespace SiYuan {

  template <typename EvalT>
    class EquationSet_Schrodinger : public panzer::EquationSet_DefaultImpl<EvalT> {

  public:

    EquationSet_Schrodinger(const Teuchos::RCP<Teuchos::ParameterList>& params,
           const int& default_integration_order,
           const panzer::CellData& cell_data,
           const Teuchos::RCP<panzer::GlobalData>& gd,
           const bool build_transient_support);

      void buildAndRegisterEquationSetEvaluators(PHX::FieldManager<panzer::Traits>& fm,
             const panzer::FieldLibrary& field_library,
             const Teuchos::ParameterList& user_data) const;

  private:
      std::string m_dof_name;
      double energy_unit_in_eV, length_unit_in_m;
      Teuchos::ParameterList pl_potential;
  };

  template<typename EvalT, typename Traits>
  class Residual_Schrodinger : public panzer::EvaluatorWithBaseImpl<Traits>,
    public PHX::EvaluatorDerived<EvalT, Traits>
  {
    public:
      Residual_Schrodinger(
        const Teuchos::ParameterList& p,
        const panzer::EvaluatorStyle&   evalStyle,
        const std::string&              resName,
        const std::string&              valName,
        const std::string&              gradName,
        const panzer::BasisIRLayout&    basis,
        const panzer::IntegrationRule&  ir );

      void postRegistrationSetup( typename Traits::SetupData d,
        PHX::FieldManager<Traits>& fm);

      void evaluateFields(typename Traits::EvalData d);

    private:
      /**
       *  \brief The scalar type.
       */
      using ScalarT = typename EvalT::ScalarT;

      const panzer::EvaluatorStyle evalStyle_;

      /**
       *  \brief A field to which we'll contribute, or in which we'll store,
       *         the result of computing this integral.
       */
      PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS> field_;

      /**
       *  \brief A field representing the vector-valued function we're
       *         integrating (\f$ \vec{s} \f$) in a 2/3-dimensional problem.
       */
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim> psiGrad_;
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> psi_;
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> V_;     // potential energy

      /**
       *  \brief The name of the basis we're using.
       */
      std::string basisName_;

      /**
       *  \brief The index in the `Workset` bases for our particular
       *         `BasisIRLayout` name.
       */
      std::size_t basisIndex_;

      //! units
      double hbar2_over_2m0;  // in energy_unit_in_eV
  }; // end of class Residual_Schrodinger

  template<typename EvalT, typename Traits>
  class Mass_Schrodinger : public panzer::EvaluatorWithBaseImpl<Traits>,
    public PHX::EvaluatorDerived<EvalT, Traits>
  {
    public:
      Mass_Schrodinger(
        const panzer::EvaluatorStyle&   evalStyle,
        const std::string&              resName,
        const std::string&              valName,
        const panzer::BasisIRLayout&    basis,
        const panzer::IntegrationRule&  ir );

      void postRegistrationSetup( typename Traits::SetupData d,
        PHX::FieldManager<Traits>& fm);

      void evaluateFields(typename Traits::EvalData d);

    private:
      /**
       *  \brief The scalar type.
       */
      using ScalarT = typename EvalT::ScalarT;

      const panzer::EvaluatorStyle evalStyle_;

      /**
       *  \brief A field to which we'll contribute, or in which we'll store,
       *         the result of computing this integral.
       */
      PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS> field_;

      /**
       *  \brief A field representing the vector-valued function we're
       *         integrating (\f$ \vec{s} \f$) in a 2/3-dimensional problem.
       */
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> DXDT_psi_;

      /**
       *  \brief The name of the basis we're using.
       */
      std::string basisName_;

      /**
       *  \brief The index in the `Workset` bases for our particular
       *         `BasisIRLayout` name.
       */
      std::size_t basisIndex_;

  }; // end of class Mass_Schrodinger


}

#include "SchrodingerEquation_impl.hpp"

#endif
