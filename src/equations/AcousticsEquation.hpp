// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _Acoustics_EquationSet_hpp_
#define _Acoustics_EquationSet_hpp_

#include <string>

#include "Teuchos_RCP.hpp"
#include "Panzer_EquationSet_DefaultImpl.hpp"
#include "Panzer_Traits.hpp"
#include "Phalanx_FieldManager.hpp"

namespace SiYuan {

  template <typename EvalT>
  class EquationSet_Acoustics : public EquationSet<EvalT>  {

  public:

    EquationSet_Acoustics(const Teuchos::RCP<Teuchos::ParameterList>& params,
           const int& default_integration_order,
           const panzer::CellData& cell_data,
           const Teuchos::RCP<panzer::GlobalData>& gd,
           const bool build_transient_support);

    void buildAndRegisterEquationSetEvaluators(PHX::FieldManager<panzer::Traits>& fm,
             const panzer::FieldLibrary& field_library,
             const Teuchos::ParameterList& user_data) const;

  private:
      std::string m_dof_names[2];
      double c_, density_;      // speed of sound
      double omega_;  // angular frequency
  };

  /* Residual of Helmholtz Equation */
  template<typename EvalT, typename Traits>
  class Residual_Helmholtz : public panzer::EvaluatorWithBaseImpl<Traits>,
    public PHX::EvaluatorDerived<EvalT, Traits>
  {
    public:
      Residual_Helmholtz(
        const panzer::EvaluatorStyle&   evalStyle,
        const panzer::BasisIRLayout&    basis,
        const panzer::IntegrationRule&  ir,
        const double& omega,
        const double& c );

      void postRegistrationSetup( typename Traits::SetupData d,
        PHX::FieldManager<Traits>& fm);

      void evaluateFields(typename Traits::EvalData d);

    private:
      /**
       *  \brief The scalar type.
       */
      using ScalarT = typename EvalT::ScalarT;
      const panzer::EvaluatorStyle evalStyle_;

    //  int numDims, numBases, numQP;
    //  double omega_;   // angular frequency = 2*pi*f
    //  double c_;       // density, speed of sound
      double wc2_;      // temp.

      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> pr_;
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> pi_;

      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim> grad_pr_;
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim> grad_pi_;

      /**
       *  \brief A field to which we'll contribute, or in which we'll store,
       *         the result of computing this integral.
       */
      PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS> fieldr_ , fieldi_;

      /**
       *  \brief The name of the basis we're using.
       */
      std::string basisName_;
      std::size_t basisIndex_;

  }; // end of class Residual_Helmholtz

  /* Residual of Time domain acoustics Equation */
  template<typename EvalT, typename Traits>
  class Residual_Acoustics : public panzer::EvaluatorWithBaseImpl<Traits>,
    public PHX::EvaluatorDerived<EvalT, Traits>
  {
    public:
      Residual_Acoustics(
        const panzer::EvaluatorStyle&   evalStyle,
        const panzer::BasisIRLayout&    basis,
        const panzer::IntegrationRule&  ir,
        const double& c, const double& );

      void postRegistrationSetup( typename Traits::SetupData d,
        PHX::FieldManager<Traits>& fm);

      void evaluateFields(typename Traits::EvalData d);

    private:
      /**
       *  \brief The scalar type.
       */
      using ScalarT = typename EvalT::ScalarT;
      const panzer::EvaluatorStyle evalStyle_;

    //  int numDims, numBases, numQP;
      double rc2_;
      double c_;          //  speed of sound
      double density_;    // density

      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> pr_;
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> dpdt_, d2pdt2_;

      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim> grad_pr_;

      /**
       *  \brief A field to which we'll contribute, or in which we'll store,
       *         the result of computing this integral.
       */
      PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS> fieldr_;

      /**
       *  \brief The name of the basis we're using.
       */
      std::string basisName_;
      std::size_t basisIndex_;

  }; // end of class Residual_Acoustics
 
}

#include "AcousticsEquation_impl.hpp"

#endif
