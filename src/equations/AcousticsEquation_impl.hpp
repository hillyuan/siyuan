// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _Acoustics_EquationSet_impl_hpp_
#define _Acoustics_EquationSet_impl_hpp_

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_StandardParameterEntryValidators.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Assert.hpp"
#include "Phalanx_DataLayout_MDALayout.hpp"
#include "Phalanx_FieldManager.hpp"

#include "Panzer_IntegrationRule.hpp"
#include "Panzer_BasisIRLayout.hpp"

// include evaluators here
#include "Panzer_Integrator_BasisTimesScalar.hpp"
#include "Panzer_Integrator_TransientBasisTimesScalar.hpp"
#include "Panzer_Integrator_BasisTimesVector.hpp"
#include "Panzer_Integrator_BasisTimesTensorTimesVector.hpp"
#include "Panzer_Product.hpp"

#include <bits/stdc++.h>   // for M_PI

// ***********************************************************************
template <typename EvalT>
SiYuan::EquationSet_Acoustics<EvalT>::
EquationSet_Acoustics(const Teuchos::RCP<Teuchos::ParameterList>& params,
    const int& default_integration_order,
    const panzer::CellData& cell_data,
    const Teuchos::RCP<panzer::GlobalData>& global_data,
    const bool build_transient_support) :
    EquationSet<EvalT> (params,default_integration_order,cell_data,global_data,build_transient_support ) 
{
  std::string model_id = params->get<std::string>("Model ID");
  std::string basis_type = "HGrad";
  int basis_order = params->get<int>("Basis Order");
  int integration_order = params->get<int>("Integration Order");
  c_ = params->get<double>("Speed of sound");

  if (!this->buildTransientSupport()) {
    m_dof_names[0] = "Pressure_Real";  // real part of P
    m_dof_names[1] = "Pressure_Img";   // imaginary part of P
    try {
      omega_ = global_data->constants.at("Frequency");
    }
    catch(std::out_of_range&) {
      std::cout << "Definition of Frequency MUST be given!" << std::endl;
    }
    omega_ = 2.0*M_PI*omega_;

    this->addDOF(m_dof_names[0],basis_type,basis_order,integration_order);
    this->addDOFGrad(m_dof_names[0]);

    this->addDOF(m_dof_names[1],basis_type,basis_order,integration_order);
    this->addDOFGrad(m_dof_names[1]);
  } else {
    density_ = params->get<double>("Density");
    m_dof_names[0] = "Pressure";
    this->enable_xdotdot();

    this->addDOF(m_dof_names[0],basis_type,basis_order,integration_order);
    this->addDOFGrad(m_dof_names[0]);
    this->addDOFTimeDerivative(m_dof_names[0]);
    this->addDOFDotDot(m_dof_names[0]);

  //  this->addC0("Density");
  //  this->addC0("Speed of sound");
  }

  this->addClosureModel(model_id);
  this->setupDOFs();
}

// ***********************************************************************
template <typename EvalT>
void SiYuan::EquationSet_Acoustics<EvalT>::
buildAndRegisterEquationSetEvaluators(PHX::FieldManager<panzer::Traits>& fm,
    const panzer::FieldLibrary& /* fl */,
    const Teuchos::ParameterList& ) const
{
  using panzer::EvaluatorStyle;

  Teuchos::RCP<panzer::IntegrationRule> ir = this->getIntRuleForDOF(m_dof_names[0]); 
  Teuchos::RCP<panzer::BasisIRLayout> basis = this->getBasisIRLayoutForDOF(m_dof_names[0]);

  // Residual
  if (this->buildTransientSupport()) 
  {
    Teuchos::RCP<PHX::Evaluator<panzer::Traits>> top =Teuchos:: rcp(new
      SiYuan::Residual_Acoustics<EvalT, panzer::Traits>(EvaluatorStyle::EVALUATES,
        *basis, *ir, c_, density_ ) );
    this->template registerEvaluator<EvalT>(fm, top);   
  }
  else {
    Teuchos::RCP<PHX::Evaluator<panzer::Traits>> top =Teuchos:: rcp(new
      SiYuan::Residual_Helmholtz<EvalT, panzer::Traits>(EvaluatorStyle::EVALUATES,
        *basis, *ir, omega_, c_ ) );
    this->template registerEvaluator<EvalT>(fm, top);
  }
}


// ***********************************************************************
/////////////////////////////////////////////////////////////////////////////
//
//  Main Constructor: Residual_Helmholtz
//
/////////////////////////////////////////////////////////////////////////////
template<typename EvalT, typename Traits>
SiYuan::Residual_Helmholtz<EvalT, Traits>::
Residual_Helmholtz(
    const panzer::EvaluatorStyle&   evalStyle,
    const panzer::BasisIRLayout&    basis,
    const panzer::IntegrationRule&  ir,
    const double& omega,
    const double& c )
    : evalStyle_(evalStyle), basisName_(basis.name())
{
    wc2_ = omega*omega /(c*c);

    pr_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP>("Pressure_Real", ir.dl_scalar);
    this->addDependentField(pr_);
    pi_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP>("Pressure_Img", ir.dl_scalar);
    this->addDependentField(pi_);

    grad_pr_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim>("GRAD_Pressure_Real", ir.dl_vector);
    this->addDependentField(grad_pr_);
    grad_pi_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim>("GRAD_Pressure_Img", ir.dl_vector);
    this->addDependentField(grad_pi_);

  //  scratch_offsets_.resize(names.size());
    
    // Create the field that we're either contributing to or evaluating
    // (storing).
    fieldr_ = PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS>("RESIDUAL_Pressure_Real",basis.functional);
    fieldi_ = PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS>("RESIDUAL_Pressure_Img",basis.functional);
    if (evalStyle == panzer::EvaluatorStyle::CONTRIBUTES) 
    {
      this->addContributedField(fieldr_);
      this->addContributedField(fieldi_);
    }
    else // if (evalStyle == EvaluatorStyle::EVALUATES)
    {
      this->addEvaluatedField(fieldr_);
      this->addEvaluatedField(fieldi_);
    }

    // Set the name of this object.
    std::string n("Helmholtz (");
    if (evalStyle == panzer::EvaluatorStyle::CONTRIBUTES)
      n += "CONTRIBUTES)";
    else // if (evalStyle == EvaluatorStyle::EVALUATES)
      n += "EVALUATES)";
  //  n += "):  " + field_.fieldTag().name();
    this->setName(n);
} // end of Main Constructor

/////////////////////////////////////////////////////////////////////////////
//
//  postRegistrationSetup()
//
/////////////////////////////////////////////////////////////////////////////
template<typename EvalT, typename Traits>
void
SiYuan::Residual_Helmholtz<EvalT, Traits>:: postRegistrationSetup(
    typename Traits::SetupData sd, PHX::FieldManager<Traits>& fm)
{
    basisIndex_ = panzer::getBasisIndex(basisName_, (*sd.worksets_)[0], this->wda);
  //  this->utils.setFieldData(bed_grad_,fm);
  //  constant.deep_copy(value);

  //  Teuchos::RCP<PHX::DataLayout> dl = Teuchos::rcp(new PHX::MDALayout<panzer::Cell, panzer::IP, panzer::Dim>(numCells,numQP,numDims));

} // end of postRegistrationSetup()

/////////////////////////////////////////////////////////////////////////////
//
//  evaluateFields()
//
/////////////////////////////////////////////////////////////////////////////
template<typename EvalT, typename Traits>
void
SiYuan::Residual_Helmholtz<EvalT, Traits>::
evaluateFields( typename Traits::EvalData workset )
{
    using Kokkos::parallel_for;
    using Kokkos::RangePolicy;
    using PHX::Device;

    typedef Intrepid2::FunctionSpaceTools<PHX::exec_space> FST;

//std::cout << "RES:" << PHX::print<EvalT>()  << std::endl;
    // Grab the basis information.
    const panzer::BasisValues2<double>& bv = *this->wda(workset).bases[basisIndex_];

    PHX::MDField<double, panzer::Cell, panzer::BASIS, panzer::IP, panzer::Dim>
          weightedGradBasis = bv.weighted_grad_basis;
    PHX::MDField<double, panzer::Cell, panzer::BASIS, panzer::IP>
          weightedBasis = bv.weighted_basis_scalar;

    int numDims = weightedGradBasis.extent(3);
    int numBases= weightedGradBasis.extent(1);
    int numQP = weightedGradBasis.extent(2);

    for( int cell=0; cell<workset.num_cells; cell++ )
    {
        for (int basis(0); basis < numBases; ++basis)
        {
          if (evalStyle_ == panzer::EvaluatorStyle::EVALUATES) {
            fieldr_(cell, basis) = 0.0;
            fieldi_(cell, basis) = 0.0;
          }
          for (int qp(0); qp < numQP; ++qp) {
            for (int dim(0); dim < numDims; ++dim) {
              fieldr_(cell, basis) += grad_pr_(cell, qp, dim) * weightedGradBasis(cell, basis, qp, dim);
              fieldi_(cell, basis) += grad_pi_(cell, qp, dim) * weightedGradBasis(cell, basis, qp, dim);
            }
            fieldr_(cell, basis) -= wc2_ * pr_(cell, qp) * weightedBasis(cell, basis, qp);
            fieldi_(cell, basis) -= wc2_ * pi_(cell, qp) * weightedBasis(cell, basis, qp);
          }
        }
    }

  //  std::cout << h_(0,0) << "," << workset.num_cells << std::endl;

} // end of evaluateFields()


// ***********************************************************************
/////////////////////////////////////////////////////////////////////////////
//
//  Main Constructor: Residual_Acoustics
//
/////////////////////////////////////////////////////////////////////////////
template<typename EvalT, typename Traits>
SiYuan::Residual_Acoustics<EvalT, Traits>::
Residual_Acoustics(
    const panzer::EvaluatorStyle&   evalStyle,
    const panzer::BasisIRLayout&    basis,
    const panzer::IntegrationRule&  ir,
    const double& c, const double& rho )
    : evalStyle_(evalStyle), basisName_(basis.name()), c_(c), density_(rho)
{
    rc2_ = rho*c*c;
    pr_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP>("Pressure", ir.dl_scalar);
    this->addDependentField(pr_);
    grad_pr_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim>("GRAD_Pressure", ir.dl_vector);
    this->addDependentField(grad_pr_);
    dpdt_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP>("DXDT_Pressure", ir.dl_scalar);
    this->addDependentField(dpdt_);
    d2pdt2_ = PHX::MDField<const ScalarT, panzer::Cell, panzer::IP>("D2XDT2_Pressure", ir.dl_scalar);
    this->addDependentField(d2pdt2_);

  //  scratch_offsets_.resize(names.size());
    
    // Create the field that we're either contributing to or evaluating
    // (storing).
    fieldr_ = PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS>("RESIDUAL_Pressure",basis.functional);
    if (evalStyle == panzer::EvaluatorStyle::CONTRIBUTES) 
    {
      this->addContributedField(fieldr_);
    }
    else // if (evalStyle == EvaluatorStyle::EVALUATES)
    {
      this->addEvaluatedField(fieldr_);
    }

    // Set the name of this object.
    std::string n("Acoustics (");
    if (evalStyle == panzer::EvaluatorStyle::CONTRIBUTES)
      n += "CONTRIBUTES)";
    else // if (evalStyle == EvaluatorStyle::EVALUATES)
      n += "EVALUATES)";
  //  n += "):  " + field_.fieldTag().name();
    this->setName(n);
} // end of Main Constructor

/////////////////////////////////////////////////////////////////////////////
//
//  postRegistrationSetup()
//
/////////////////////////////////////////////////////////////////////////////
template<typename EvalT, typename Traits>
void
SiYuan::Residual_Acoustics<EvalT, Traits>:: postRegistrationSetup(
    typename Traits::SetupData sd, PHX::FieldManager<Traits>& fm)
{
    basisIndex_ = panzer::getBasisIndex(basisName_, (*sd.worksets_)[0], this->wda);
  //  this->utils.setFieldData(bed_grad_,fm);
  //  constant.deep_copy(value);

  //  Teuchos::RCP<PHX::DataLayout> dl = Teuchos::rcp(new PHX::MDALayout<panzer::Cell, panzer::IP, panzer::Dim>(numCells,numQP,numDims));

} // end of postRegistrationSetup()

/////////////////////////////////////////////////////////////////////////////
//
//  evaluateFields()
//
/////////////////////////////////////////////////////////////////////////////
template<typename EvalT, typename Traits>
void
SiYuan::Residual_Acoustics<EvalT, Traits>::
evaluateFields( typename Traits::EvalData workset )
{
    using Kokkos::parallel_for;
    using Kokkos::RangePolicy;
    using PHX::Device;

    typedef Intrepid2::FunctionSpaceTools<PHX::exec_space> FST;

//std::cout << "RES:" << PHX::print<EvalT>()  << std::endl;
    // Grab the basis information.
    const panzer::BasisValues2<double>& bv = *this->wda(workset).bases[basisIndex_];

    PHX::MDField<double, panzer::Cell, panzer::BASIS, panzer::IP, panzer::Dim>
          weightedGradBasis = bv.weighted_grad_basis;
    PHX::MDField<double, panzer::Cell, panzer::BASIS, panzer::IP>
          weightedBasis = bv.weighted_basis_scalar;

    int numDims = weightedGradBasis.extent(3);
    int numBases= weightedGradBasis.extent(1);
    int numQP = weightedGradBasis.extent(2);

    for( int cell=0; cell<workset.num_cells; cell++ )
    {
        for (int basis(0); basis < numBases; ++basis)
        {
          if (evalStyle_ == panzer::EvaluatorStyle::EVALUATES) {
            fieldr_(cell, basis) = 0.0;
          }
          for (int qp(0); qp < numQP; ++qp) {
            for (int dim(0); dim < numDims; ++dim) {
              fieldr_(cell, basis) += grad_pr_(cell, qp, dim)/density_ * weightedGradBasis(cell, basis, qp, dim);
            }
            fieldr_(cell, basis) += d2pdt2_(cell, qp)/rc2_ * weightedBasis(cell, basis, qp);
          }
        }
    }

  //  std::cout << h_(0,0) << "," << workset.num_cells << std::endl;

} // end of evaluateFields()

#endif 
