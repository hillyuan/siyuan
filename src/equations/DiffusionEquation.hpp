// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef DIFFUSION_EQUATION_HPP
#define DIFFUSION_EQUATION_HPP

#include <vector>
#include <string>

#include "Teuchos_RCP.hpp"
#include "EquationSet_Impl.hpp"
#include "Panzer_Traits.hpp"
#include "Phalanx_FieldManager.hpp"

#include "MaterialBase.hpp"
#include "MaterialEvaluator.hpp"

namespace SiYuan {
	
/*
 *  Convection-Diffusion Equation
 */
template <typename EvalT>
class EquationSet_Diffusion : public EquationSet<EvalT> {

  public:    

    EquationSet_Diffusion(const Teuchos::RCP<Teuchos::ParameterList>& params,
		       const int& default_integration_order,
		       const panzer::CellData& cell_data,
		       const Teuchos::RCP<panzer::GlobalData>& gd,
		       const bool build_transient_support);
    
    void buildAndRegisterEquationSetEvaluators(PHX::FieldManager<panzer::Traits>& fm,
					const panzer::FieldLibrary& field_library,
					const Teuchos::ParameterList& user_data) const;

  private:
      std::string m_prefix;
      std::string m_dof_name;
      std::string m_matl_name;
      bool do_convection;
      bool has_source;
  };

}

#include "DiffusionEquation_impl.hpp"

#endif
