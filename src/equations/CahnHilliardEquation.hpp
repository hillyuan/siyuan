// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _CahnHilliard_EquationSet_hpp_
#define _CahnHilliard_EquationSet_hpp_

#include <string>

#include "Teuchos_RCP.hpp"
#include "Panzer_EquationSet_DefaultImpl.hpp"
#include "Panzer_Traits.hpp"
#include "Phalanx_FieldManager.hpp"

// ref: Garcke, Rumpf, and Weikard : The Cahn-Hilliard Equation with Elasticity - Finite Element Approximation 
// and Qualitative Studies,  Interfaces and Free Boundaries, August 2000

namespace SiYuan {

  template <typename EvalT>
  class EquationSet_CahnHilliard : public EquationSet<EvalT>  {

  public:

    EquationSet_CahnHilliard(const Teuchos::RCP<Teuchos::ParameterList>& params,
           const int& default_integration_order,
           const panzer::CellData& cell_data,
           const Teuchos::RCP<panzer::GlobalData>& gd,
           const bool build_transient_support);

    void buildAndRegisterEquationSetEvaluators(PHX::FieldManager<panzer::Traits>& fm,
             const panzer::FieldLibrary& field_library,
             const Teuchos::ParameterList& user_data) const;

  private:
      std::string m_dof_names[2];
      double beta_;
  };

  template<typename EvalT, typename Traits>
  class Residual_ChemistryPotential : public panzer::EvaluatorWithBaseImpl<Traits>,
    public PHX::EvaluatorDerived<EvalT, Traits>
  {
    public:
      Residual_ChemistryPotential(
        const panzer::EvaluatorStyle&   evalStyle,
        const std::string&              resName,
        const std::string&              valName,
        const std::string&              gradName,
        const panzer::BasisIRLayout&    basis,
        const panzer::IntegrationRule&  ir,
        const std::vector<std::string>& c3 );

      void postRegistrationSetup( typename Traits::SetupData d,
        PHX::FieldManager<Traits>& fm);

      void evaluateFields(typename Traits::EvalData d);

    private:
      /**
       *  \brief The scalar type.
       */
      using ScalarT = typename EvalT::ScalarT;

      const panzer::EvaluatorStyle evalStyle_;

      /**
       *  \brief A field to which we'll contribute, or in which we'll store,
       *         the result of computing this integral.
       */
      PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS> field_;

      /**
       *  \brief A field representing the vector-valued function we're
       */
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim> cGrad_;  // grad of concentration
    //  PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> mu_;
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> m_freeEnergy;  // chemistry potential 

      /**
       *  \brief Material properties.
       */
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> gamma_;   // Gradient Energy Coefficient

      /**
       *  \brief The name of the basis we're using.
       */
      std::string basisName_;

      /**
       *  \brief The index in the `Workset` bases for our particular
       *         `BasisIRLayout` name.
       */
      std::size_t basisIndex_;

  }; // end of class Residual_CahnHilliard

  template<typename EvalT, typename Traits>
  class Residual_Concentration : public panzer::EvaluatorWithBaseImpl<Traits>,
    public PHX::EvaluatorDerived<EvalT, Traits>
  {
    public:
      Residual_Concentration(
        const panzer::EvaluatorStyle&   evalStyle,
        const std::string&              resName,
        const std::string&              valName,
        const std::string&              gradName,
        const panzer::BasisIRLayout&    basis,
        const panzer::IntegrationRule&  ir,
        const std::vector<std::string>& c3 );

      void postRegistrationSetup( typename Traits::SetupData d,
        PHX::FieldManager<Traits>& fm);

      void evaluateFields(typename Traits::EvalData d);

    private:
      /**
       *  \brief The scalar type.
       */
      using ScalarT = typename EvalT::ScalarT;

      const panzer::EvaluatorStyle evalStyle_;

      /**
       *  \brief A field to which we'll contribute, or in which we'll store,
       *         the result of computing this integral.
       */
      PHX::MDField<ScalarT, panzer::Cell, panzer::BASIS> field_;

      /**
       *  \brief A field representing the vector-valued function we're
       *         integrating (\f$ \vec{s} \f$) in a 2/3-dimensional problem.
       */
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP, panzer::Dim> muGrad_;  // grad of potential
    //  PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> mu_;
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> cdot_;  // dc/dt 

      /**
       *  \brief Material properties.
       */
      PHX::MDField<const ScalarT, panzer::Cell, panzer::IP> M_diff_;   // Diffusion Mobility

      /**
       *  \brief The name of the basis we're using.
       */
      std::string basisName_;

      /**
       *  \brief The index in the `Workset` bases for our particular
       *         `BasisIRLayout` name.
       */
      std::size_t basisIndex_;

  }; // end of class Residual_CahnHilliard
  
}

#include "CahnHilliardEquation_impl.hpp"

#endif
