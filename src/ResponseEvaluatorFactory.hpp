#ifndef __ResponseEvaluatorFactory_hpp__
#define __ResponseEvaluatorFactory_hpp__

#include <string>

#include "PanzerAdaptersSTK_config.hpp"
#include "Panzer_ResponseEvaluatorFactory_Functional.hpp"

namespace SiYuan {

/** This class defines a response based on a functional. */
  
template <typename EvalT,typename LO,typename GO> 
class ResponseEvaluatorFactory : public panzer::ResponseEvaluatorFactory_Functional<EvalT,LO,GO> 
{
public:

   ResponseEvaluatorFactory(MPI_Comm comm, int cubatureDegree, std::string name)
     : panzer::ResponseEvaluatorFactory_Functional<EvalT,LO,GO>(comm,cubatureDegree,false), m_dof_name(name)
   {}

   virtual ~ResponseEvaluatorFactory() {}
   
   /** Build and register evaluators for a response on a particular physics
     * block. 
     *
     * \param[in] responseName The name of the response to be constructed
     *                         by these evaluators.
     * \param[in,out] fm Field manager to be fuild with the evaluators.
     * \param[in] physicsBlock What physics block is being used for constructing
     *                         the evaluators
     * \param[in] user_data The user data parameter list, this stores things
     *                      that the user may find useful.
     */
   virtual void buildAndRegisterEvaluators(const std::string & responseName,
                                           PHX::FieldManager<panzer::Traits> & fm,
                                           const panzer::PhysicsBlock & physicsBlock,
                                           const Teuchos::ParameterList & user_data) const;

private:
  std::string  m_dof_name;

};

struct Response_Builder {
  MPI_Comm comm;
  int cubatureDegree;
  std::string dof_name;

  template <typename T>
  Teuchos::RCP<panzer::ResponseEvaluatorFactoryBase> build() const
  { return Teuchos::rcp(new ResponseEvaluatorFactory<T,int,int>(comm,cubatureDegree,dof_name)); }
};

}

#include "ResponseEvaluatorFactory_impl.hpp"

#endif
