// @HEADER
//***********************************************************************//
//    SiYuan: A numerical PDE solver                                     //
//    This Software is released under the BSD 2-Clause license detailed  //
//    in the file "LICENSE" in the top-level SiYuan directory            //
//***********************************************************************//
// @HEADER

#ifndef SCHRODINGERPOTENTIAL_IMPL_HPP
#define SCHRODINGERPOTENTIAL_IMPL_HPP

#include "Teuchos_TestForException.hpp"
#include "Phalanx_DataLayout.hpp"
#include "Sacado_ParameterRegistration.hpp"
//#include "Panzer_ExprEval_impl.hpp"

template<typename EvalT, typename Traits>
SiYuan::SchrodingerPotential<EvalT, Traits>::
SchrodingerPotential(Teuchos::ParameterList& p, const panzer::IntegrationRule&  ir)
{
  // ********************
  // Validate and parse parameter list
  // ********************
  {
    Teuchos::RCP<Teuchos::ParameterList> validPL = Teuchos::rcp(new Teuchos::ParameterList);

    validPL->set<std::string>("Type", "defaultType", "Switch between different potential types");
    validPL->set<double>("E0", 1.0, "Energy scale - dependent on type");
    validPL->set<double>("Scaling Factor", 1.0, "Constant scaling factor");
    validPL->set<Teuchos::Array<double> >("Coordinate Origin", Teuchos::tuple<double>( 0.0, 0.0, 0.0 ),
        "Origin of current coordinate system.");

    // For string formula potential
    validPL->set<std::string>("Formula", "", "Mathematical expression containing x,y,z that specifies the potential at coordinates (x,y,z).  In 1D and 2D use just x and x,y respectively.");
  
    // For Finite Wall potential 
    validPL->set<double>("Barrier Effective Mass", 0.0, "Barrier effective mass in [m0]");
    validPL->set<double>("Barrier Width", 0.0, "Barrier width in length_unit_in_m");
    validPL->set<double>("Well Effective Mass", 0.0, "Well effective mass in [m0]");
    validPL->set<double>("Well Width", 0.0, "Well width in length_unit_in_m");

    // For 1D MOSCapacitor to test the 1D P-S iteration
    /* specific parameters for 1D MOSCapacitor to set correct effective mass 
     for oxide and silicon regions (could be given in the Poisson Coupling
     section, putting here for less perturbation to the main code). */

    validPL->set<double>("Oxide Width", 0.0, "Oxide width in length_unit_in_m");
    validPL->set<double>("Silicon Width", 0.0, "Silicon width in length_unit_in_m");

    validPL->set<double>("Energy unit in eV", 1.0, "Oxide width in length_unit_in_m");
    validPL->set<double>("Length unit in m", 1.0, "Silicon width in length_unit_in_m");

    // For potential imported from a state
    validPL->set<std::string>("State Name", "", "Name of state to import potential from");

    p.validateParametersAndSetDefaults(*validPL);
  //  p.validateParameters(*validPL,0);
  }

  numQPs  = ir.dl_vector->extent_int(1);
  numDims = ir.dl_vector->extent_int(2);
  ir_degree = ir.order();

  energy_unit_in_eV = p.get<double>("Energy unit in eV");
  length_unit_in_m = p.get<double>("Length unit in m");
  
  potentialType = p.get("Type", "defaultType");
  E0 = p.get("E0", 1.0);
  scalingFactor = p.get("Scaling Factor", 1.0);
  
  // Parameters for String Formula
  stringFormula = p.get("Formula", "");

  // Parameters for Finite Wall 
  barrEffMass = p.get<double>("Barrier Effective Mass", 0.0);
  barrWidth = p.get<double>("Barrier Width", 0.0);
  wellEffMass = p.get<double>("Well Effective Mass", 0.0);
  wellWidth = p.get<double>("Well Width", 0.0);
  
  potentialStateName = p.get<std::string>("State Name","Not Specified");
  origin = p.get<Teuchos::Array<double> >("Coordinate Origin");

  // Add E0 as a Sacado-ized parameter
/*  Teuchos::RCP<ParamLib> paramLib =
      p.get< Teuchos::RCP<ParamLib> >("Parameter Library", Teuchos::null);

  this->registerSacadoParameter("Schrodinger Potential E0", paramLib);
  this->registerSacadoParameter("Schrodinger Potential Scaling Factor", paramLib);*/

  m_V = PHX::MDField<ScalarT, panzer::Cell, panzer::IP>("Potential", ir.dl_scalar);
  this->addEvaluatedField(m_V);
  this->setName( "Schrodinger Potential" + PHX::print<EvalT>() );
}

// **********************************************************************
template<typename EvalT, typename Traits>
void SiYuan::SchrodingerPotential<EvalT, Traits>::
postRegistrationSetup(typename Traits::SetupData sd,
                      PHX::FieldManager<Traits>& fm)
{
  this->utils.setFieldData(m_V,fm);
  ir_index = panzer::getIntegrationRuleIndex(ir_degree,(*sd.worksets_)[0], this->wda);
}

// **********************************************************************
template<typename EvalT, typename Traits>
void SiYuan::SchrodingerPotential<EvalT, Traits>::
evaluateFields(typename Traits::EvalData workset)
{
  // Parabolic potential (test case)
  if (potentialType == "Parabolic") 
  {
    /***** define universal constants as double constants *****/
    const double hbar = 1.0546e-34;  // Planck constant [J s]
    const double emass = 9.1094e-31; // Electron mass [kg]
    const double evPerJ = 6.2415e18; // eV per Joule (J/eV)

    const double parabolicFctr = 0.5 * (emass / (evPerJ * hbar*hbar) );

    // prefactor from constant, including scaling due to units
    ScalarT prefactor = parabolicFctr * E0*E0 * (energy_unit_in_eV * pow(length_unit_in_m,2));  
prefactor = 1.0;
    for (std::size_t cell = 0; cell < workset.num_cells; ++cell) {
      for (std::size_t qp = 0; qp < numQPs; ++qp) {
        double r2 = 0.0;
        for (std::size_t i=0; i<numDims; i++) {
          r2 += (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,i)-origin[i])*
            (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0)-origin[i]);
        }
        m_V(cell, qp) = scalingFactor * prefactor * r2;
      }
    }
  }
  
  // Infinite barrier wall
  else if (potentialType == "Infinite Wall")
  {
    m_V.deep_copy(ScalarT(0.0));
  } 

  // Finite barrier wall
  else if (potentialType == "Finite Wall")
  {
    for (std::size_t cell = 0; cell < workset.num_cells; ++cell)
    {
      for (std::size_t qp = 0; qp < numQPs; ++qp)
        m_V(cell, qp) = finiteWallPotential(workset, numDims, cell,qp);
    }    
  }

  // Specifed by formula string
  else if (potentialType == "String Formula")
  {
    for (std::size_t cell = 0; cell < workset.num_cells; ++cell)
    {
      for (std::size_t qp = 0; qp < numQPs; ++qp)
        m_V(cell, qp) = stringFormulaPotential(workset, numDims, cell,qp);
    }    
  }

  
  // Potential energy taken from a StateManager state
  else if (potentialType == "From State") 
  {
  /*  Albany::StateArray& states = *workset.stateArrayPtr;
    Albany::MDArray& potentialState = states[potentialStateName];

    for (std::size_t cell=0; cell < workset.numCells; ++cell) {
      for (std::size_t qp=0; qp < numQPs; ++qp) {
        double d =  potentialState(cell, qp);
        V(cell, qp) = d; //scalingFactor * d;
      }
    }*/
  }    

  /***** otherwise error? ******/
  else 
  {
    TEUCHOS_TEST_FOR_EXCEPT(true);
  }
}

// **********************************************************************
template<typename EvalT,typename Traits>
typename SiYuan::SchrodingerPotential<EvalT,Traits>::ScalarT& 
SiYuan::SchrodingerPotential<EvalT,Traits>::getValue(const std::string &n)
{
  if(n == "Schrodinger Potential Scaling Factor") return scalingFactor;
  else if(n == "Schrodinger Potential E0") return E0;
  else TEUCHOS_TEST_FOR_EXCEPT(true); return E0; //dummy so all control paths return
}


// **********************************************************************

//Return potential in energy_unit_in_eV * eV units
template<typename EvalT,typename Traits>
typename SiYuan::SchrodingerPotential<EvalT,Traits>::ScalarT
SiYuan::SchrodingerPotential<EvalT,Traits>::finiteWallPotential( typename Traits::EvalData workset, 
   const int numDim, const int cell, const int qp )
{
  ScalarT val;
  
  switch (numDim)
  {
    case 1: // 1D: total width = 2*barrWidth + wellWidth
    {
      if ( (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) >= 0) &&
        (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) < barrWidth) )
        val = E0;  // barrier
      else if ( (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) >= barrWidth) && 
        (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) <= (barrWidth+wellWidth)) )
        val = 0;   // well
      else if ( (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) > (barrWidth+wellWidth)) && 
        (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) <= (2*barrWidth+wellWidth)) )
        val = E0;  // barrier
      else 
        TEUCHOS_TEST_FOR_EXCEPTION (true, Teuchos::Exceptions::InvalidParameter, std::endl 
			    << "Error! x coordinate is outside [0, 2*barrWidth+wellWidth] range,"
			    << " make sure 1D Scale in Discretization equal to 2*barrWidth+wellWidth !" << std::endl);
      break;
    }
    
    case 2: // 2D
    {
      if ( (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) >= barrWidth) && 
        (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) <= (barrWidth+wellWidth)) &&
        (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,1) >= barrWidth) && 
        (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,1) <= (barrWidth+wellWidth)) )
        val = 0.0;  // well
      else
        val = E0;   // barrier
      break;
    }

    case 3: // 3D
    {
      if ( (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) >= barrWidth) && 
           (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0) <= (barrWidth+wellWidth)) &&
           (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,1) >= barrWidth) && 
           (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,1) <= (barrWidth+wellWidth)) && 
           (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,2) >= barrWidth) && 
           (this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,2) <= (barrWidth+wellWidth)) )
        val = 0.0;  // well
      else
        val = E0;   // barrier
      break;
    }
    
    default: 
    {
      TEUCHOS_TEST_FOR_EXCEPTION (true, Teuchos::Exceptions::InvalidParameter, std::endl 
			  << "Error! Invalid numDim = " << numDim << ", must be 1 or 2 or 3 !" << std::endl);
			break;  
    }
    
  }  // end of switch (numDim)
  
  return scalingFactor * val;
}


// **********************************************************************

//Return potential in energy_unit_in_eV * eV units
template<typename EvalT,typename Traits>
typename SiYuan::SchrodingerPotential<EvalT,Traits>::ScalarT
SiYuan::SchrodingerPotential<EvalT,Traits>::stringFormulaPotential( typename Traits::EvalData workset, 
  const int numDim, const int cell, const int qp )
{
  ScalarT val;
  ScalarT x, y, z;
  
  switch (numDim)
  {
    case 1: // 1D: total width = 2*barrWidth + wellWidth
    {
      x = this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0);
      y = 0; z=0;
      break;
    }
    
    case 2: // 2D
    {
      x = this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0);
      y = this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,1);
      z = 0;
      break;
    }

    case 3: // 3D
    {
      x = this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,0);
      y = this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,1);
      z = this->wda(workset).int_rules[ir_index]->ip_coordinates(cell,qp,2);
      break;
    }
    
    default: 
    {
      TEUCHOS_TEST_FOR_EXCEPTION (true, Teuchos::Exceptions::InvalidParameter, std::endl 
			  << "Error! Invalid numDim = " << numDim << ", must be 1 or 2 or 3 !" << std::endl);
			break;  
    }    
  }  // end of switch (numDim)


  /*panzer::Expr::Eval<double*> eval;
  panzer::Expr::set_cmath_functions(eval);
  Teuchos::any result;

  eval.set("x", x);
  eval.set("y", y);
  eval.set("z", z);
  eval.read_string(result, stringFormula, stringFormula);*/

//  result = Evaluate(stringFormula,x,y,z); //Parse and evaluate formula string 
//  val = result;                           // (could speed this up in future by saving intermediate parse info)
  return scalingFactor * val;
}


// *****************************************************************************

#endif