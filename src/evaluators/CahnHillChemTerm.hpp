// @HEADER
//***********************************************************************//
//    SiYuan: A numerical PDE solver                                     //
//    This Software is released under the BSD 2-Clause license detailed  //
//    in the file "LICENSE" in the top-level SiYuan directory            //
//***********************************************************************//
// @HEADER

#ifndef _CAHNHILLCHEMTERM_HPP
#define _CAHNHILLCHEMTERM_HPP

#include "Phalanx_config.hpp"
#include "Phalanx_Evaluator_WithBaseImpl.hpp"
#include "Phalanx_Evaluator_Derived.hpp"
#include "Phalanx_MDField.hpp"

#include "Teuchos_ParameterList.hpp"
#include "Sacado_ParameterAccessor.hpp"
#include "Teuchos_Array.hpp"

#include "Panzer_Dimension.hpp"
#include "Panzer_FieldLibrary.hpp"
#include "Panzer_Evaluator_WithBaseImpl.hpp"

namespace SiYuan {
	
/** 
 * \brief  Chemical energy density defined in: H. Garcke, B. Niethammer, M, Rumpf, U. Weikard: 
 *    Transient Coarsening Behaviour In The Cahn-Hilliard Model, Acta Materialia, 2003
 */
template<typename EvalT, typename Traits>
class CahnHillChemTerm : public panzer::EvaluatorWithBaseImpl<Traits>,
  	public PHX::EvaluatorDerived<EvalT, Traits>
{
public:
  	typedef typename EvalT::ScalarT ScalarT;

  	CahnHillChemTerm(Teuchos::ParameterList& p,
                 const panzer::IntegrationRule&  ir);
  
  	void postRegistrationSetup(typename Traits::SetupData d,
			     PHX::FieldManager<Traits>& vm);
  
  	void evaluateFields(typename Traits::EvalData d);
  
private:

	int ir_degree, ir_index;
  	std::size_t numQPs;

	//! input: Concentration & Gibbs free energy
	PHX::MDField<const ScalarT,panzer::Cell, panzer::IP> m_rho;
	PHX::MDField<const ScalarT,panzer::Cell, panzer::IP> m_mu;
  	//! output: Chemistry free energy depends upon concentration
    PHX::MDField<ScalarT, panzer::Cell, panzer::IP> m_freeEnergy; //potential 

    double m_beta;   // 0<\beta<=1.0 in equation (1.2)
};

}

#include "CahnHillChemTerm_impl.hpp"

#endif
