// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#include "NeumannDef.hpp"

#include <string>

namespace SiYuan {

//**********************************************************************
/*NeumannBoundary::NeumannBoundary(const Teuchos::ParameterList& params
      , const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh)
{
  m_sideset_name = params.get<std::string>("Sideset Name");
  m_eblock_name = params.get<std::string>("Element Block Name");
  // if sideset exists
  std::vector<stk::mesh::Entity> sides;
  try {
    mesh->getMySides(m_sideset_name,m_eblock_name,sides);
  } 
  catch(panzer_stk::STK_Interface::SidesetException & e) {
      std::stringstream ss;
      std::vector<std::string> sideSets; 
      mesh->getSidesetNames(sideSets);
 
      ss << e.what() << "\nChoose one of:\n";
      for(std::size_t i=0;i<sideSets.size();i++) 
        ss << "\"" << sideSets[i] << "\"\n";

      TEUCHOS_TEST_FOR_EXCEPTION_PURE_MSG(true,std::logic_error,ss.str());
  }

  m_group_id = params.get<int>("Group ID");
  m_dof_name = params.get< std::string >("Variable Name");
//  m_bc_type = params.get< std::string >("Type");

  m_pl_value = params.sublist("Data");
};

//=======================================================================
void 
readNeumannBoundary( std::vector< NeumannBoundary >& bcs, const Teuchos::ParameterList& pl,
           const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh)
{
  bcs.clear();
  
  std::size_t bc_index = 0;
  for (Teuchos::ParameterList::ConstIterator bc_pl=pl.begin(); bc_pl != pl.end(); ++bc_pl,++bc_index) {
    TEUCHOS_TEST_FOR_EXCEPTION( !(bc_pl->second.isList()), std::logic_error,
				"Error - All objects in the Neumann Conditions sublist must be sublists!" );
    Teuchos::ParameterList& sublist = Teuchos::getValue<Teuchos::ParameterList>(bc_pl->second);
    
    NeumannBoundary bc(sublist, mesh);
    bcs.emplace_back(bc);
  }
}*/


//**********************************************************************
CLoad::CLoad(const Teuchos::ParameterList& params
      , const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh)
{
  std::string nd = params.get<std::string>("Nodeset Name");

  int ndid;
  try {
    ndid = std::stoi(nd);
    m_nodes_gid.emplace_back(ndid);
  }
  catch (...) {
    mesh->getAllNodeSetIds(nd,m_nodes_gid);
  }

/*  try {
    mesh->getAllNodeSetsId(m_nodeset_name,m_nodes_gid);
  } 
  catch(panzer_stk::STK_Interface::NodesetException & e) {
      std::stringstream ss;
      std::vector<std::string> nodeSets; 
      mesh->getNodesetNames(nodeSets);
 
      ss << e.what() << "\nChoose one of:\n";
      for(std::size_t i=0;i<nodeSets.size();i++) 
        ss << "\"" << nodeSets[i] << "\"\n";

      TEUCHOS_TEST_FOR_EXCEPTION_PURE_MSG(true,std::logic_error,ss.str());
  }*/

  m_group_id = params.get<int>("Group ID");
  m_dof_name = params.get< std::string >("Variable Name");
//  m_bc_type = params.get< std::string >("Type");

  m_params = params.sublist("Data");
//  m_value = pl_value.get< double >("Value");
//  m_value = XYZLib::variable<double>(pl_value);
};

//=======================================================================
void 
readCLoad( std::vector< CLoad >& bcs, const Teuchos::ParameterList& pl,
           const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh)
{
  bcs.clear();
  
  std::size_t bc_index = 0;
  for (Teuchos::ParameterList::ConstIterator bc_pl=pl.begin(); bc_pl != pl.end(); ++bc_pl,++bc_index) {
    TEUCHOS_TEST_FOR_EXCEPTION( !(bc_pl->second.isList()), std::logic_error,
				"Error - All objects in the Neumann Conditions sublist must be sublists!" );
    Teuchos::ParameterList& sublist = Teuchos::getValue<Teuchos::ParameterList>(bc_pl->second);
    
    CLoad bc(sublist, mesh);
    bcs.emplace_back(bc);
  }
}


}
