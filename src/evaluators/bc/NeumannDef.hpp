// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _BC_NEUMANNDEF_HPP_
#define _BC_NEUMANNDEF_HPP_

#include "Teuchos_ParameterList.hpp"
#include "Panzer_Traits.hpp"
#include "Panzer_WorksetDescriptor.hpp"
#include "Panzer_EvaluatorsRegistrar.hpp"
#include "Panzer_STK_Interface.hpp"

#include <string>
#include <map>

namespace SiYuan {

//**********************************************************************
//**********************************************************************
struct NeumannBoundary
{
    unsigned int m_group_id;     // group ID
    std::string  m_sideset_name;
    std::string  m_eblock_name;
  //  Teuchos::Array<std::string>  m_dof_name;
    std::string  m_dof_name;
    std::string  m_bc_type;
    Teuchos::ParameterList  m_pl_value;  

    NeumannBoundary(const Teuchos::ParameterList& params, const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh);

    panzer::WorksetDescriptor getWorksetDescriptor() const
    { 
      panzer::WorksetDescriptor desc(m_eblock_name,m_sideset_name);
      return desc; 
    }
};

//**********************************************************************
//  Concentrated load
//**********************************************************************
struct CLoad
{
    unsigned int m_group_id;     // group ID
    std::vector<stk::mesh::EntityId> m_nodes_gid;
    std::string  m_dof_name;
    std::string  m_bc_type;
    Teuchos::ParameterList m_params;

    CLoad(const Teuchos::ParameterList& params, const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh);
};

void readNeumannBoundary( std::vector< NeumannBoundary >& bcs, const Teuchos::ParameterList& p
                       , const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh );

void readCLoad( std::vector< CLoad >& bcs, const Teuchos::ParameterList& p
              , const Teuchos::RCP<const panzer_stk::STK_Interface>& mesh );

}

#endif
