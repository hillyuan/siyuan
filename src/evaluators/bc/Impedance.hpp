// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _IMPEDANCE_HPP
#define _IMPEDANCE_HPP

#include "Panzer_Dimension.hpp"
#include "Phalanx_Evaluator_Macros.hpp"
#include "Phalanx_MDField.hpp"

#include "Panzer_Evaluator_Macros.hpp"

#include <complex>

using panzer::Cell;
using panzer::Point;
using panzer::Dim;

//**********************************************************************
//  Typical boundary conditions in Acoustics analysis
//**********************************************************************

namespace SiYuan {

    
//! Impedance: flux = j * \omega \rho /Z
template<typename EvalT, typename Traits>
class AcousticImpedance
  :
  public panzer::EvaluatorWithBaseImpl<Traits>,
  public PHX::EvaluatorDerived<EvalT, Traits>
{
  public:

    AcousticImpedance(const Teuchos::ParameterList& p);

    void postRegistrationSetup( typename Traits::SetupData d,
      PHX::FieldManager<Traits>& fm);

    void evaluateFields(typename Traits::EvalData d);

  private:

    using ScalarT = typename EvalT::ScalarT;

    std::complex<double> z_, tempz;
    double omega_;
    double density_;  // should be material property
    PHX::MDField<const ScalarT> r_pressure_, i_pressure_;
    PHX::MDField<ScalarT> r_flux_, i_flux_;

    int num_cell, space_dim, num_cub_points, num_basis;
    std::string basis_name;
    std::size_t basis_index;
}; 

//! Impedance: flux = j * \omega \rho *v
template<typename EvalT, typename Traits>
class AcousticVelocity
  :
  public panzer::EvaluatorWithBaseImpl<Traits>,
  public PHX::EvaluatorDerived<EvalT, Traits>
{
  public:

    AcousticVelocity(const Teuchos::ParameterList& p);

    void postRegistrationSetup( typename Traits::SetupData d,
      PHX::FieldManager<Traits>& fm);

    void evaluateFields(typename Traits::EvalData d);

  private:

    using ScalarT = typename EvalT::ScalarT;

    std::complex<double> v_;
    double omega_;
    double density_;  // should be material property
    double wp;        // temp
    PHX::MDField<ScalarT> r_flux_, i_flux_;

    int num_cell, space_dim, num_cub_points, num_basis;
    std::string basis_name;
    std::size_t basis_index;
}; 



}

#include "Impedance_impl.hpp"

#endif
