// @HEADER
//***********************************************************************//
//    SiYuan: A numerical PDE solver                                     //
//    This Software is released under the BSD 2-Clause license detailed  //
//    in the file "LICENSE" in the top-level SiYuan directory            //
//***********************************************************************//
// @HEADER

#ifndef _BCSTRATEGY_FACTORY_HPP
#define _BCSTRATEGY_FACTORY_HPP

#include "Teuchos_RCP.hpp"
#include "Panzer_Traits.hpp"
#include "Panzer_BCStrategy_TemplateManager.hpp"
#include "Panzer_BCStrategy_Factory.hpp"
#include "Panzer_BCStrategy_Factory_Defines.hpp"
#include "Panzer_GlobalData.hpp"

// Add my bcstrategies here
#include "BCStrategy_Dirichlet_Constant.hpp"
#include "BCStrategy_Neumann.hpp"

namespace SiYuan {
  
  PANZER_DECLARE_BCSTRATEGY_TEMPLATE_BUILDER(
    BCStrategy_Dirichlet_Constant, BCStrategy_Dirichlet_Constant)
  PANZER_DECLARE_BCSTRATEGY_TEMPLATE_BUILDER(
    BCStrategy_Neumann, BCStrategy_Neumann)

  struct BCFactory : public panzer::BCStrategyFactory {

    Teuchos::RCP<panzer::BCStrategy_TemplateManager<panzer::Traits> >
    buildBCStrategy(const panzer::BC& bc, const Teuchos::RCP<panzer::GlobalData>& global_data) const
    {

      Teuchos::RCP<panzer::BCStrategy_TemplateManager<panzer::Traits> > bcs_tm = 
	      Teuchos::rcp(new panzer::BCStrategy_TemplateManager<panzer::Traits>);
      
      bool found = false;

      PANZER_BUILD_BCSTRATEGY_OBJECTS("Constant", BCStrategy_Dirichlet_Constant)
      PANZER_BUILD_BCSTRATEGY_OBJECTS("Flux", BCStrategy_Neumann)
      PANZER_BUILD_BCSTRATEGY_OBJECTS("Vector Flux", BCStrategy_Neumann)
      PANZER_BUILD_BCSTRATEGY_OBJECTS("Convection", BCStrategy_Neumann)
      PANZER_BUILD_BCSTRATEGY_OBJECTS("Radiate", BCStrategy_Neumann)
      PANZER_BUILD_BCSTRATEGY_OBJECTS("Acoustics Velocity", BCStrategy_Neumann)
      PANZER_BUILD_BCSTRATEGY_OBJECTS("Impedance", BCStrategy_Neumann)

      TEUCHOS_TEST_FOR_EXCEPTION(!found, std::logic_error, 
			 "Error - the BC Strategy called \"" << bc.strategy() <<
			 "\" is not a valid identifier in the BCStrategyFactory.  Either add a valid implementation to your factory or fix your input file.  The relevant boundary condition is:\n\n" << bc << std::endl);
      
      return bcs_tm;

    }

  };
  
}

#endif
