// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#include "SolutionObserver.hpp"
#include "SolutionObserver_impl.hpp"

namespace SiYuan {

  template class SolutionObserver<double>;

}
