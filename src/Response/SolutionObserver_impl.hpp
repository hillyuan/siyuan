// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef SOLUTION_OBSERVER_IMPL_HPP
#define SOLUTION_OBSERVER_IMPL_HPP

#include "Piro_NOXSolver.hpp"
#include "Piro_RythmosSolver.hpp"
#include "Piro_TempusSolver.hpp"
#include "Piro_LOCASolver.hpp"

namespace SiYuan {

  template<typename ScalarT>
  void
  SolutionObserver<ScalarT>::observeCurrentStep()
  {
    if( method_=="Steady" ) {
      auto solver = Teuchos::rcp_dynamic_cast< Piro::NOXSolver<ScalarT>  >(piro_) ->getSolver();
      Teuchos::RCP<const Thyra::VectorBase<double> > th_x = solver->get_current_x(); 
    } else if ( method_=="Rythmos" ) {
      auto solver = Teuchos::rcp_dynamic_cast< Piro::RythmosSolver<ScalarT>  >(piro_) ->getTimeStepSolver();
      Teuchos::RCP<const Thyra::VectorBase<double> > th_x = solver->get_current_x(); 
    } else if ( method_=="Tempus" ) {
      auto solver = Teuchos::rcp_dynamic_cast< Piro::TempusSolver<ScalarT>  >(piro_) ->getSolver();
      Teuchos::RCP<const Thyra::VectorBase<double> > th_x = solver->get_current_x(); 
    } else if ( method_=="LOCA" ) {
      auto solver = Teuchos::rcp_dynamic_cast< Piro::LOCASolver<ScalarT>  >(piro_) ->getSolver();
      const NOX::Abstract::Vector& x = solver->getSolutionGroup().getX();
      const NOX::Thyra::Vector* n_th_x = dynamic_cast<const NOX::Thyra::Vector*>(&x);
      TEUCHOS_TEST_FOR_EXCEPTION(n_th_x == NULL, std::runtime_error, "Failed to dynamic_cast to NOX::Thyra::Vector!")
      Teuchos::RCP<const Thyra::VectorBase<double> > th_x = n_th_x->getThyraRCPVector(); 
    }
  }

}

#endif
