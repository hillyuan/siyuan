// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef __Response_impl_hpp__
#define __Response_impl_hpp__

namespace SiYuan {

template <typename EvalT>
Thyra::ArrayRCP<double> 
Response<EvalT> :: getThyraVector() const
{
   Teuchos::ArrayRCP<double> data;
   Teuchos::rcp_dynamic_cast<Thyra::SpmdVectorBase<double> >(tVector_,true)->getNonconstLocalData(Teuchos::outArg(data));

   return data;
}

template <typename EvalT>
Teuchos::RCP<const Thyra::VectorSpaceBase<double> > 
Response<EvalT> :: getVectorSpace() const
{
  // lazily build the space and return it
  if(vSpace_==Teuchos::null) {
    if(this->vectorIsDistributed())
      vSpace_ = Thyra::defaultSpmdVectorSpace<double>(tComm_,this->localSizeRequired(),-1);
    else
      vSpace_ = Thyra::locallyReplicatedDefaultSpmdVectorSpace<double>(tComm_,this->localSizeRequired());
  }

  return vSpace_;
}

}

#endif
