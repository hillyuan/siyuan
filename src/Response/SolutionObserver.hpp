// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef SOLUTION_OBSERVER_HPP
#define SOLUTION_OBSERVER_HPP

#include "Thyra_AdaptiveSolutionManager.hpp"
#include "Thyra_ModelEvaluatorDefaultBase.hpp"

#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

namespace SiYuan {

  template<typename EvalT>
  class SolutionObserver : public Thyra::AdaptiveSolutionManager {

  public:
  //	SolutionManager(
  //      const Teuchos::RCP<Teuchos::ParameterList>& appParams );
		
	  void observeCurrentStep();

  private:
  	std::string method_;
    Teuchos::RCP<Thyra::ModelEvaluatorDefaultBase<double> > piro_;
  };

}

#include "SolutionObserver_impl.hpp"

#endif
