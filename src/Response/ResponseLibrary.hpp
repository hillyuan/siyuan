// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef __ResponseLibrary_hpp__
#define __ResponseLibrary_hpp__

#include "Panzer_WorksetContainer.hpp"
#include "Panzer_WorksetDescriptor.hpp"
#include "Panzer_GlobalIndexer.hpp"
#include "Panzer_LinearObjFactory.hpp"
#include "Panzer_STK_Interface.hpp"

#include "ResponseBase.hpp"

namespace SiYuan {

template <typename TraitsT>
class ResponseLibrary {
public:
  ResponseLibrary(const Teuchos::RCP<panzer::WorksetContainer> & wc,
                   const Teuchos::RCP<const panzer::GlobalIndexer> & ugi,
                   const Teuchos::RCP<const panzer::LinearObjFactory<TraitsT> > & lof,
                   const Teuchos::RCP<const panzer_stk::STK_Interface> & mesh); 

private:
  Teuchos::RCP<panzer::WorksetContainer> wkstContainer_;
  Teuchos::RCP<const panzer::GlobalIndexer> globalIndexer_;
  Teuchos::RCP<const panzer::LinearObjFactory<TraitsT> > linObjFactory_;
  Teuchos::RCP<const panzer_stk::STK_Interface> mesh_;

  std::vector< Teuchos::RCP<ResponseBase> > reponses_;
};

}

#endif
