// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _TEMPUS_OBSERVER_FACTORY_HPP
#define _TEMPUS_OBSERVER_FACTORY_HPP

#include "PanzerAdaptersSTK_config.hpp"

#include "Panzer_ResponseLibrary.hpp"
#include "Panzer_Traits.hpp"

// Individual Observers
#include "TempusObserver_WriteToExodus.hpp"
#include "Tempus_IntegratorObserverComposite.hpp"

namespace SiYuan {

  class TempusObserverFactory  {

  public:
    TempusObserverFactory(const Teuchos::RCP<panzer::ResponseLibrary<panzer::Traits> > & stkIOResponseLibrary,
                           const Teuchos::RCP<panzer::WorksetContainer> wkstContainer,
                           bool useCoordinateUpdate)
       : stkIOResponseLibrary_(stkIOResponseLibrary)
       , wkstContainer_(wkstContainer)
       , useCoordinateUpdate_(useCoordinateUpdate) 
    {}

    bool useNOXObserver() const { return false; }
    
    Teuchos::RCP<Tempus::IntegratorObserver<double> >
    buildTempusObserver(const Teuchos::RCP<panzer_stk::STK_Interface>& mesh,
			 const Teuchos::RCP<const panzer::GlobalIndexer> & dof_manager,
			 const Teuchos::RCP<const panzer::LinearObjFactory<panzer::Traits> >& lof,
       const int& freq ) const
    {
      // note: Composite observer loops in the order added
      Teuchos::RCP<Tempus::IntegratorObserverComposite<double> > composite_observer =
        Teuchos::rcp(new Tempus::IntegratorObserverComposite<double>);

      {
        Teuchos::RCP<TempusObserver_WriteToExodus> observer 
            = Teuchos::rcp(new TempusObserver_WriteToExodus(mesh,dof_manager,lof,freq,stkIOResponseLibrary_));
        composite_observer->addObserver(observer);
      }

      return composite_observer;
    }

  private:
    //! Store STK IO response library...be careful, it will be modified externally
    Teuchos::RCP<panzer::ResponseLibrary<panzer::Traits> > stkIOResponseLibrary_;

    Teuchos::RCP<panzer::WorksetContainer> wkstContainer_;

    //! Use the coordinate update observer?
    bool useCoordinateUpdate_;
  };

}

#endif
