// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _RYTHMOS_OBSERVER_COORDINATEUPDATE_HPP
#define _RYTHMOS_OBSERVER_COORDINATEUPDATE_HPP

#include "Rythmos_StepperBase.hpp"
#include "Rythmos_IntegrationObserverBase.hpp"
#include "Rythmos_TimeRange.hpp"

#include "Teuchos_RCP.hpp"
#include "Teuchos_Assert.hpp"

#include "Panzer_WorksetContainer.hpp"

namespace SiYuan {

  class RythmosObserver_CoordinateUpdate : 
    public Rythmos::IntegrationObserverBase<double> {

  public:
    
    RythmosObserver_CoordinateUpdate(const Teuchos::RCP<panzer::WorksetContainer> & workset_container) :
      m_workset_container(workset_container)
    { }
    
    Teuchos::RCP<Rythmos::IntegrationObserverBase<double> >
    cloneIntegrationObserver() const
    { return Teuchos::rcp(new RythmosObserver_CoordinateUpdate(m_workset_container)); }

    void resetIntegrationObserver(const Rythmos::TimeRange<double>& /* integrationTimeDomain */)
    { }

    void observeCompletedTimeStep(const Rythmos::StepperBase<double>& /* stepper */,
				  const Rythmos::StepControlInfo<double>& /* stepCtrlInfo */,
				  const int /* timeStepIter */)
    { TEUCHOS_ASSERT(m_workset_container!=Teuchos::null); 
      m_workset_container->clear(); }
    
  protected:

    Teuchos::RCP<panzer::WorksetContainer> m_workset_container;
  };

}

#endif
