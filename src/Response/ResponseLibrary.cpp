// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER


#include "ResponseLibrary.hpp"

namespace SiYuan {

template <typename TraitsT>
ResponseLibrary<TraitsT>::ResponseLibrary(const Teuchos::RCP<panzer::WorksetContainer> & wc,
                   const Teuchos::RCP<const panzer::GlobalIndexer> & ugi,
                   const Teuchos::RCP<const panzer::LinearObjFactory<TraitsT> > & lof,
                   const Teuchos::RCP<const panzer_stk::STK_Interface> & mesh)
{
  wkstContainer_ = wc;
  globalIndexer_ = ugi;
  linObjFactory_ = lof;
  mesh_ = mesh;
};

}
