#include <Teuchos_XMLParameterListCoreHelpers.hpp>
#include <Teuchos_YamlParameterListCoreHelpers.hpp>
#include <cassert>


int main(int argc, char** argv) {
  for (int i = 1; i < argc; ++i) {
    std::string yamlFileName(argv[i]);
    auto params = Teuchos::getParametersFromYamlFile(yamlFileName);
    auto baseName = yamlFileName.substr(0, yamlFileName.length() - 5);
    auto xmlFileName = baseName + ".xml";
    Teuchos::writeParameterListToXmlFile(*params, xmlFileName);
  }
}
