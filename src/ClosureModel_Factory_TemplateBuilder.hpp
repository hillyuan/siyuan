// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER


#ifndef _CLOSURE_MODEL_FACTORY_TEMPLATE_BUILDER_HPP
#define _CLOSURE_MODEL_FACTORY_TEMPLATE_BUILDER_HPP

#include <string>
#include "Sacado_mpl_apply.hpp"
#include "Teuchos_RCP.hpp"
#include "ClosureModel_Factory.hpp"

namespace SiYuan {

  class ClosureModelFactory_TemplateBuilder {
    Teuchos::RCP<const panzer::LinearObjFactory<panzer::Traits> > distr_param_lof;

  public:

    void setDistributedParameterLOF(const Teuchos::RCP<const panzer::LinearObjFactory<panzer::Traits> > & dpl)
    { distr_param_lof = dpl; }
    
    template <typename EvalT>
    Teuchos::RCP<panzer::ClosureModelFactoryBase> build() const {
      Teuchos::RCP<SiYuan::ClosureModelFactory<EvalT> > closure_factory =
          Teuchos::rcp(new SiYuan::ClosureModelFactory<EvalT>) ;
      closure_factory->setDistributedParameterLOF(distr_param_lof);

      return Teuchos::rcp_static_cast<panzer::ClosureModelFactoryBase>(closure_factory);
    }
    
  };
  
}

#endif 
