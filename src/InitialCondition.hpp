// @HEADER
//*********************************************************************//
//  SiYuan: A numerical PDE solver                                     //
//  Copyright (2022) YUAN Xi                                           //
//  This Software is released under the BSD 2-Clause license detailed  //
//  in the file "LICENSE" in the top-level SiYuan directory            //
//*********************************************************************//
// @HEADER

#ifndef _INITIAL_CONDITION_HPP
#define _INITIAL_CONDITION_HPP

#include "Panzer_LinearObjFactory.hpp"
#include "Panzer_Traits.hpp"
#include "Panzer_PhysicsBlock.hpp"
#include "Panzer_STK_Interface.hpp"

#include "Thyra_ModelEvaluatorDefaultBase.hpp"

#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

namespace SiYuan {

  template<typename ScalarT>
  void InitialConditions(Teuchos::ParameterList&, 
    Thyra::ModelEvaluatorDefaultBase<ScalarT> & model,
    Teuchos::RCP<panzer_stk::STK_Interface>& mesh,
	  Teuchos::RCP<panzer::LinearObjFactory<panzer::Traits> >& lof,
    Teuchos::RCP<panzer::PhysicsBlock>& physicsBlock);

}

#endif
