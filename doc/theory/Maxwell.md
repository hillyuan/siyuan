# Maxwell Equation

$$\begin{aligned}
\nabla \times H =J + \frac{\partial D}{\partial t} \\
\nabla \times E = -\frac{\partial B}{\partial t} \\
\nabla \cdot B =0 \\
\nabla \cdot D = \rho \\
\end{aligned}  \tag{1} $$

combined with the material laws
$$\begin{aligned}
B=\mu H +\mu_0 M \\
D =\varepsilon E + P \\
J=J_c+j_i=\sigma (E+v \times B) + j_i \\
\end{aligned}$$

Charge continuity equation 
$$ \nabla \cdot J + \frac{\partial \rho}{\partial t}=0$$
is a  consequence of (1.2) by taking divergence of (1.2) and substituting (1.3) into it.

Symbol | Name  | SI Units  
  ----------|------|--------    
  H | magnetic field | Ampere/meter
  B | magnetic flux density | Tesla
  E | electric field | Volt/meter
  D | electric displacement | Coulomb/$meter^2$
  J | current density | Ampere/$meter^2$
  $\rho$ | charge density | Coulomb/$meter^3$
  M | magnetization | Vs/$m^2$
  P | electric polarization | As/$m^2$

There is eight equations in above Maxwell equations to solve six unknowns (vector $E$ and $B$). Introducing material laws and taking divergence of (1.1) and (1.2) yields
$$ \nabla \cdot \frac{\partial B}{\partial t} = \frac{\partial}{\partial t}\nabla \cdot B=0;  \\  \frac{\partial \nabla \cdot D}{\partial t}+ \nabla \cdot J=0 $$
Using the continuity equation, we arrive at
$$ \frac {\partial}{\partial t}(\nabla \cdot D-\rho)=0$$
from which 
$$ \nabla \cdot B=c_1;  \nabla \cdot D-\rho = c_2$$
Maxwell equation (1.3) and (1.4) tell us $c_1=0; c_2=0$. They are needed to uniquely determiane a solution, but they are needed only as initial condition and not as part of the system of linear independent differential equations. They are sed to find the initial state of longitudinal component of fields, then equation (1.3) and (1.4) dictate the time evolution of the longitudinal components at all future times.

## A-$\varphi$ Potential Formulation

Because $\nabla \cdot B=0$, there exists a vector potential A such that
$$ B= \nabla \times A $$.
Pluggig in Ampere's law leads to
$$ \nabla \times (\mu^{-1}\nabla \times A)= \varepsilon \frac{\partial E}{\partial t}+\sigma E +j_i $$
For panzer::minifem
$$ \varepsilon \frac{dE}{dt} = \mu^{-1} \nabla \times B - j_i - \sigma E $$ 

Our goal is to get rid of the E. We only want to have one variable A on the left
hand side. Reformulating Faraday’s law leads to
$$ \nabla \times E = -\frac{\partial}{\partial t} \nabla \times A  \longrightarrow \nabla \times (E+ \frac{\partial A}{\partial t})=0$$
which means 
$$ \exists \varphi: E=-\nabla \varphi+\frac{\partial A}{\partial t}$$
Therefore, (1.1) & (1.2) can be expressed by
$$ \nabla \times (\mu^{-1}\nabla \times A) + \varepsilon \frac{\partial^2 A}{\partial t^2}+\sigma \frac{\partial A}{\partial t} =j_1 - \sigma\nabla \varphi - \varepsilon \frac{\partial \nabla \varphi}{\nabla t}$$

We see, that for any arbitrary scalar function $\phi$  the potentials
$$ \tilde{A} = A+\nabla \psi;  \quad \tilde{\varphi} = \varphi- \frac{\partial \psi}{\partial t}$$
satisfy the equation above (plugging in and using Schwarz). By choosing a vector
potential $A^*$ such that
$$ A^* =A+\int_{t_0}^t\nabla\varphi d\tilde{t} $$
we get
$$ E=-\frac{\partial A^*}{\partial t};  \quad \nabla \times A = \nabla \times A^* $$
For convenience, we can introduce
$$ E=- \frac{\partial A}{\partial t} $$
and finally obtain the vector potential formulation of Maxwell’s equations
$$ \nabla \times (\mu^{-1}\nabla A)+\varepsilon \frac{\partial^2 A}{\partial t^2}+\sigma \frac{\partial A}{\partial t}=j_1$$
Once $\tilde{A}$ is determined, the magnetic and electric fields can be computed as follows:
$$\begin{aligned}
B= \nabla \times \tilde{A} \\
E = -\frac{\partial \tilde{A}}{\partial t} - \nabla \psi = -\frac{\partial \tilde{A}}{\partial t}\\
H= \mu B \\
D = \varepsilon E \\
\end{aligned}$$

## E-field based Formulation
$$\nabla \times \mu^{-1}B =J + \varepsilon \frac{\partial B}{\partial t} $$
Differentiating it in time leads to second order equation in time
$$ \varepsilon \frac{\partial ^2E}{\partial t^2} = \mu^{-1} \nabla \times \frac{\partial B}{\partial t} - \sigma \frac{\partial E}{\partial t} = = \frac{\partial j_i}{\partial t} -\mu^{-1} \nabla \times \nabla \times E - \sigma \frac{\partial E}{\partial t} $$ 
or 
$$ \varepsilon \frac{\partial^2E}{\partial t^2} + \sigma \frac{\partial E}{\partial t}+ \nabla \times (\mu^{-1} \nabla \times E) = -\frac{\partial j_i}{\partial t}$$

The Dirichlet and Robin-like boundary conditions for this equation are
$$\begin{aligned}
E \times n = f \times n\quad   on\quad   \Gamma_E \\
n \times \nabla\times E + \alpha n\times(n\times\frac{\partial E}{\partial t})=g \quad on \quad \Gamma_Z
\end{aligned}$$

Note that the Neumann boundary condition is achieved by setting $\alpha=0$. An absorbing boundary condition can be created by choosing
$$ \alpha=\sqrt{\varepsilon_0\mu_0} $$
To obtain the variational formulation of (21.1) integration by parts is carried out. After using (21.3) we then come to the weak form
$$
\begin{aligned}
 \int_\Omega(\mu^{-1}curlE \cdot curlv+\varepsilon \frac{\partial^2E}{\partial t^2} \cdot v + \sigma \frac{\partial E}{\partial t} \cdot v)+ \alpha \int_{\Gamma_Z} (\mu^{-1}(\frac{\partial E}{\partial t} \times n)\cdot(v \times n)dS \\
 = -\int_{\Gamma_Z}\mu^{-1}g \cdot vdS - \int_\Omega J \cdot v d\Omega
\end{aligned}$$
which can be used to obtain the fully discrete equations

## B-field based Formulation
$$\nabla \times E = -\frac{\partial B}{\partial t} $$
Taking the divengence of equation (1.1)
$$ \nabla \times \mu^{-1} \nabla \times B =\nabla \times J + \varepsilon \frac{\partial}{\partial t} (\nabla \times E) = \nabla \times (\sigma E + j_i) - \varepsilon\frac{\partial^2 B}{\partial t^2}$$
or 
$$ \varepsilon \frac{\partial^2B}{\partial t^2} + \sigma\frac{\partial B}{\partial t} + \nabla \times (\mu^{-1} \nabla \times B) = \nabla \times j_i$$

# Special Regimes
There are many different special cases of Maxwell’s equations.

<img src="img/electromagnetism.PNG" width=70% alt="hello" title="Split-up of elementromagnetsim(TD=Time Domain, FD=Frequency Domain)">

## Time-Harmonic Problem

In this section we assume linear material laws and time-harmonic excitation with the frequency ω. This leads to the following ansatz for the variable U(x, t), where U stands for any of the quantities H, B, E, D, J, ρ and Re is the projection onto the real part:
$$U(x,t)=Re(\hat{U}(x)e^{i\omega t})$$
This separation ansatz yields
$$\begin{aligned}
\frac{\partial U}{\partial t} = i \omega U\\
\frac{\partial^2 U}{\partial t^2} = -\omega^2 U \\
\end{aligned}$$
With the assumptions one can derive a time-harmonic variant of the Maxwell equations

$$\begin{aligned}
\nabla \times H(x) =j_i(x) + (i \omega\varepsilon+\sigma)E(x) \\
\nabla \times E(x) = -i \omega \mu H(x) \\
\nabla \cdot \mu H(x) =0 \\
\nabla \cdot \varepsilon E(x) = \rho (x)\\
\end{aligned}$$

and the continuity equation
$$ i\omega\rho(x) + \nabla \cdot J(x)=0 $$
In the vector potential formulation we have $E=-i\omega A$ and $B=\nabla \times A$. so above system simplifies to
$$ \nabla \times(\mu^{-1}\nabla \times A)+(i\omega\sigma-\omega^2\varepsilon)A=j_i$$
where A is unknown and $j_i$ is known. The term $(i\omega\sigma-\omega^2\varepsilon)=:\kappa$ is depending on the type of problem.
In the E-field based formulation we get
$$ \nabla \times(\mu^{-1}\nabla \times E)+(i\omega\sigma-\omega^2\varepsilon)E=-i\omega j_i$$
In the B-field based formulation we get
$$ \nabla \times (\nabla \times H) + \mu(i\sigma\omega - \varepsilon \omega^2 ) H = \nabla \times j_i$$

$$\omega^2\varepsilon - i\omega\sigma =\omega^2\varepsilon_c; \varepsilon_c=\varepsilon-i\sigma/\omega =\varepsilon_0(\varepsilon_r-i \frac{\sigma}{\omega\varepsilon_0})=\varepsilon_0\varepsilon_{cr} $$

## Quasistatic Case – Eddy Current Problem
In this regime we assume that the frequency is low, i.e., we will neglect the displacement currents. In other words |∂D/∂t| << |J|. Further, we obtain the so-called eddy-current approximation to the Maxwell equations.

$$\begin{aligned}
\nabla \times H =J  \\
\nabla \times E = -\frac{\partial B}{\partial t} \\
\nabla \cdot B =0 \\
\nabla \cdot D = \rho \\
\end{aligned}$$

Then, the vector potential formulation from Section 3.1 in the time domain reads as
$$ \sigma\frac{\partial\tilde{A}}{\partial t}+ \nabla \times(\mu^{-1}\nabla \times \tilde{A})=j_i $$
and in the frequency domain
$$ i\omega\sigma \tilde{A}+ \nabla \times(\mu^{-1}\nabla \times \tilde{A})=j_i $$
The E-field based formulation in the time domain is
$$ \sigma\frac{\partial E}{\partial t}+ \nabla \times(\mu^{-1}\nabla \times E)= -\frac{\partial j_i}{\partial t} $$
and in the frequency domain
$$ i\omega\sigma E+ \nabla \times(\mu^{-1}\nabla \times E)=-i\omega j_i $$

## Electrostatic Case
In this case we assume that all involved quantities are time-independent. Then the equations for electric and magnetic fields decouple. The electric field is irrotational and its sources are charges. The Maxwell equations reduce to
$$\begin{aligned}
\nabla \times E=0  \\
\nabla \cdot D=\rho
\end{aligned}$$
there exists a sufficiently smooth potential field $\phi$ with $E=\nabla \phi$. This electric potential has units of Volts in the SI system and is very convenient for practical applications because it is a scalar quantity. Further, by setting $E=\nabla \phi$ Faraday’s law in the electrostatic case is automatically satisfied.
Assuming linear material law and using the equation (1) gives
$$-\nabla \cdot \varepsilon \nabla \phi = \rho - \nabla \cdot P$$
which is Poisson's equation for the electric potential, where we have assumed a linear constitutive relation between D  and E  of the form D =ϵE +P . This allows a polarization which is proportional to E  as well as a polarization independent of E . If this relation happens to be nonlinear then Poisson's equation would need to be replaced with a more complicated nonlinear expression.

The solutions to equation are non unique because they can be shifted by any additive constant. This means that we must apply a Dirichlet boundary condition at least at one point in the problem domain in order to obtain a solution. Typically this point will be on the boundary but it need not be so. Such a Dirichlet value is equivalent to fixing the voltage (a.k.a. potential) at one or more locations. Additionally, this equation admits a normal derivative boundary condition. This corresponds to setting n⋅D to a prescribed value on some portion of the boundary. This is equivalent to defining a surface charge density on that portion of the boundary.



1) Boundary conditions

For electric potential either Dirichlet or Neumann boundary condition can be used. The Dirichlet boundary condition gives the value of the potential on specified boundaries. The Neumann boundary condition is used to give a flux condition on specified boundaries

$$-\varepsilon \nabla \phi \cdot n =g$$
The flux may be defined e.g. by the surface charge density: $g=\sigma$.
In case there is a object in infinite space it is of course not possible to extent the volume over it. Instead a spherically symmetric approximation may be used. It results to a flux given by
$$g=\varepsilon \phi \frac{r \cdot n}{r^2}$$
Conductors are often covered by thin oxidation layers which may contain static charges. The effect of
these charges can be taken into account by Robin type of boundary condition which combines the fixed
potential value on the conductor and the flux condition due to the static charges
$$g=\varepsilon \frac{\varepsilon_h}{h}\phi + \frac{1}{2} \rho h + \frac{\varepsilon_h}{h} \Phi_0$$
where $carepsilon_h$ and h are the permittivity and the thickness of the oxidation layer respectively, $\rho$ is the static charge density of the layer, and $Phi_0$ is the fixed potential on the conductor.
Note that this formulation is valid only for thin layers. For a larger layer a separate body should be added and a source defined for that.

2) Capacitance matrix

## Static Current Conduction

In the electroquasistatic approximation Maxwell’s equations are written as
$$\begin{aligned}
\nabla \times H =J + \frac{\partial D}{\partial t} \\
\nabla \times E \simeq 0 \\
\nabla D = \rho \\
\end{aligned}$$
so that the electric field may be expressed in terms of an electric scalar potential $\phi$ as
$$ E=-\nabla \phi$$
In addition, the continuity equation for electric charges is easily obtained from (1) and (3):
$$ \frac{\partial \rho}{\partial t}+ \nabla \cdot J=0 $$
The Ohm’s law for conducting material gives the relationship between current density and electric field,
$$ J = \sigma E $$
where $\sigma$ is the electric conductivity. Starting from the continuity equation (17.5) and using the equations (17.6) and (17.4) we get
$$ -\nabla \cdot \nabla \phi - \nabla \cdot \varepsilon \nabla \frac{d\phi}{dt} = \rho_t $$
This Poisson equation is used to solve the electric potential. The source term is often zero but in some cases it might be necessary. The volume current density is now calculated by
$$ J = -\sigma \nabla \phi $$
and electric power loss density which is turned into heat by
$$h=\nabla \phi \cdot \sigma \nabla \phi$$
The latter is often called the Joule heating. The total heating power is found by integrating the above equation over the conducting volume.
The user may also compute the nodal heating which is just the integral of the heating to nodes.

1) Boundary Conditions
For electric potential either Dirichlet or Neumann boundary condition can be used. The Dirichlet boundary condition gives the value of the potential on specified boundaries. The Neumann boundary condition is used to give a current $J_b$ on specified boundaries
$$ J_b=\sigma \nabla \phi \cdot n$$
2) Power and current control
Sometimes the desired power or current of the system is known a priori. The control may be applied to the system. When the electric potential has been computed the heating power may be estimated from
$$ P = \int_\Omega \nabla \phi \cdot \nabla \phi d\Omega$$
If there is a potential difference U in the system the effective resistance may also be computed from $R =U^2/P$ and the effective current from $I = P/U$.
The control is achieved by multiplying the potential and all derived fields by a suitable variable. For power control the coefficient is
$$C_p = \sqrt{P_0/P}$$
where $P_0$ is the desired power. For current control the coefficient is
$$C_I=I_0/I$$
where $I_0$ is the desire total current.

## The Magnetostatic Case

In this case we assume that all involved quantities are time-independent. Then the equations for electric and magnetic fields decouple and the Maxwell equations reduce to
$$\begin{aligned}
\nabla \times H=J  \\
\nabla \cdot B=\rho
\end{aligned}$$

The magnetic induction is solenoidal and the magnetic field intensity possesses curls at positions where a current appears.

Magnetostatics describes the time-independent magnetic fields.  The magnetic field may be created by electromagnets  with  given  current  distributions  or  permanent  ferromagnets.

We will again assume a somewhat more general constitutive relation between H  and B  than is normally seen:
$$ B=\mu H + \mu_0 M = \mu_0(1+\chi_M)H +\mu_0 M $$
Where the magnetization is split into two portions; one which is proportional to H  and given by $\chi_M H$ , and another which is independent of H and is given by M . This allows for paramagnetic and/or diamagnetic materials defined through μ as well as ferromagnetic materials represented by M . This choice yields:
$$ \nabla \times \mu^{-1} B = J+\nabla\times\mu^{-1}\mu_0 M$$
Which, when combined with Equation (1.4), becomes:
$$ \nabla \times \mu^{-1} \nabla \times A = J+\nabla\times\mu^{-1}\mu_0 M$$

If J happens to be zero we have another option because we can assume that $H=-\nabla\varphi_M$ for some scalar potential $\varphi_M$. When combined with equation (1.4) this leads to:
$$ \nabla \cdot \mu\nabla\varphi_M= \nabla \cdot \mu_0\nabla M$$

The vector potential is again non unique so we must apply additional constraints in order to arrive at a solution for A . When adopting Coulomb gauge, it is $$\nabla \cdot A=0$$, In this case, the partial differential equation and the boundary conditions of the static magnetic field problem can be wriiten ad follows:
$$\begin{aligned}
\nabla \times \mu^{-1} \nabla \times A - \nabla( \mu^{-1}\nabla \cdot A) = J+\nabla\times\mu^{-1}\mu_0 M \\
(\mu^{-1} \nabla \times A +I) \times n =K on \Gamma_H \\
A \cdot n=0 on \Gamma_H \\
n \times A = \alpha on \Gamma_B \\
\mu^{-1}\nabla \cdot A=0 on \Gamma_B
\end{aligned}$$
The formulation is the so-called gauged A-formulation.

When working analytically it is common to constrain the solution by restricting the divergence of A but numerically this leads to other complications. For our problems of interest it will be necessary to require Dirichlet boundary conditions on the entire outer surface in order to sufficiently constrain the solution.

Dirichlet boundary conditions for the vector potential on a surface provide a means to specify the component of B  normal to that surface. For example, setting the tangential components of A to be zero on a particular surface results in a magnetic flux density which must be tangent to that surface.

If there are conductors in the system the electric field is obtained from
$$ E=σ\frac{∂A}{∂t} $$
where σ is the electric conductivity. In time-harmonic case the current density is sinusoidal $j=j_0e^{iωt}$,where ω= 2πf is the angular frequency. Using a trial $A=A_0e^{iωt}$, we obtain an equation for the amplitude
$$ \nabla \times \mu^{-1} \nabla \times A_0 +i\omega\sigma A_0 = j_0$$
If the geometry is axisymmetric, then the magnetic flux density B has only r- and z-components, and the current density j and the vector potential A only φ-components, and
$$ \nabla \times \mu^{-1} \nabla \times A_\phi e_\phi +i\omega\sigma A_\phi e_\phi = j_\phi e_\phi$$
The current density is given as a body force with the keyword Current Density.  The vector potential satisfies now automatically the Coulomb gauge. In stady state case Aφ is real and there is only one unknown. In the harmonic case the equation has two unknowns – the in-phase and the out-of-phase component of the vector potential.  After solution the heat generation in the conductors may be computed from
$$ h=\frac{1}{2}\sigma\omega^2|A_0|^2 $$
The magnetic flux density is calculated as a post-processing step from the vector potential. Both the vector potential and the magnetic flux density components are written in the result and ElmerPost files. The variable names in the result file are magnetic vector potential and magnetic flux density 1,2 and 3. By definition, magnetostatics deals with steady-state problems.  However, the problem may be solved nominally time-dependent. This merely means that it is solved for a set of given current densities.

# Variational Princple
The common structure we have seen therefore is
$$ \nabla \times(\mu^{-1}\nabla \times A)+\kappa A = f $$
The weak form of this equation is 
$$ \int_\Omega(\nabla \times(\mu^{-1}\nabla \times A) )\cdot v d\Omega + \int_\Omega A \cdot vd\Omega = \int_\Omega f \cdot vd\Omega $$
Using integration by parts for the main part leads to
$$ \int_\Omega(\nabla \times(\nabla \times A) )\cdot v d\Omega = \int_\Omega \nabla \times A \cdot \nabla \times vd\Omega - \int_{\partial \Omega}(\nabla \times A \times n)\cdot vdS$$
Thus, we obtain the variational formulation
$$ \int_\Omega \mu^{-1}\nabla \times A \cdot \nabla \times vd\Omega + \int_\Omega A \cdot vd\Omega = \int_\Omega f \cdot vd\Omega + \int_{\partial \Omega}(\mu^{-1}\nabla \times A \times n)\cdot vdS$$

The boundary integral in the weak form is
$$\int_{\partial \Omega}(\mu^{-1}\nabla \times A \times n)\cdot vdS$$
In case of A is vector potential, $B=\nabla \times A$. The boundary values in the square brackets correspondto the toroidal component of the magnetic field strength.
$$\mu^{-1}\nabla \times A \times n = H \times n = K_0$$

$$\nabla \times A=\begin{bmatrix}0 & -\frac{\partial N}{\partial z} & \frac{\partial N}{\partial y} \\ \frac{\partial N}{\partial z} & 0 & -\frac{\partial N}{\partial x}\\ -\frac{\partial N}{\partial y} & \frac{\partial N}{\partial x} & 0 \end{bmatrix}\begin{array}{ccc} \left( \begin{array}{ccc} A_x\\ A_y \\ A_z \end{array} \right) \end{array} $$


# Reference:
1) [Some Benchmark Problems in Electromagnetics](http://qiita.com/drafts/c686397e4a0f4f11683d)