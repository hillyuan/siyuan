# Finite Element Analysis of the Schrodinger Equation
   <b>Table of contents </b>
   - <a name="postulates_sec">Postulates of Quantum Mechanics. </a>
   - <a name="timeind_sec">Time Indepedent Schroginer Equation </a>
   - <a name="weakform_sec">Weak Formulation </a>
   - <a name="1DBox">The Particle in a 1D Box  </a>

## [Postulates of Quantum Mechanics](#postulates_sec)
- **Postulate 1**
The state of a quantum mechanical system is completely specified by a function $\psi(x; t)$, which depends on the space and time coordinates of the particle. This function, called the wave function or state function, has the important property that its norm. $\psi^*(x; t)\psi(x; t)dv$ is the probability that the particle lies in the volume element dv located at x at time t. The wavefunction must satisfy certain mathematical conditions because of this probabilistic interpretation. For the case of a single particle, the probability of finding it somewhere is 1. So we have the normalization condition:
 $$\int_{-\infty}^{\infty}\psi^*(x,t)\psi(x,t)dv=1 \tag{1}$$
 - **Postulate 2**
To every observable, A, in classical mechanics (e.g. energy and momentum) there corresponds a linear Hermitian operator, $\hat{A}$, in quantum mechanics.
- **Postulate 3**
In any measurement of the observable associated with operator $\hat{A}$, the only values that will ever be observed are the eigenvalues a, which satisfy the eigenvalue equation:
$$\hat{A}\psi_a=a\psi_a \tag{2}$$
where $\psi_a$ is the eigenfunction associated with the eigenvalue a of the operator $\hat{A}$. This postulate captures the central point of quantum mechanics that values of dynamical variables can be quantized. If the system is in an eigenstate of $\hat{A}$ with eigenvalue a, then any measurement of the quantity A will yield a. Although measurements must always yield an eigenvalue, the state does not have to be an eigenstate of $\hat{A}$. An arbitrary state can be expanded in the complete set of eigenvectors of $\hat{A}(\hat{A}\psi_a=a\psi_a)$ as:
$$\psi=\sum_i^n c_i\psi_i \tag{3}$$
In this case we only know that the measurement of A will yield one of
the values ai with $a_i$ probability $|c_i|^2$
- **Postulate 4**
If a system is in a state described by a normalized wave function $\psi$, then the average value of the observable corresponding to $\hat{A}$ is given by:
$$<A>=\int_{-\infty}^{\infty}\psi^*(x,t)\hat{A}\psi(x,t)dv \tag{4}$$
- **Postulate 5**
The wavefunction of a system evolves in time according to the time-dependent Schrodinger equation:
$$i \hbar \frac{\partial \psi}{\partial t} = H(x,p)\psi(x,t)$$
where m is the mass of the particle,$\hbar=h/2\pi$ is the Planck constant and Hamilton H(x,p) is function of position x and momentum p. In the case of no magnetic field. $ p=-i\hbar\nabla $. Using the fact that the energy of a particle is the sum of kinetic and potential parts,
$$ H= \frac{p^2}{2m}+ V(x) $$
The Schrodinger equation then becames
$$i \hbar \frac{\partial \psi}{\partial t} = -\frac{\hbar^2}{2m}\nabla^2\psi(x,t)+V(x) \psi(x,t) \tag{5}$$
V (x) is a real function representing the potential energy of the system. Although the time-independent Schrodinger equation can be derived through elementary methods, the time-dependent version can not be derived so must be accepted as a fundamental postulate of quantum mechanics.

## [Time Indepedent Schroginer Equation](#timeind_sec)
A common simplification is done by expressing the wavefunction as a product of spatial and temporal terms.  If the potential energy is assumed to be independent of time, then the wavefunction becomes,
$$ \psi(x,t)=f(t)u(x) $$
Equation (5) can be simplified as
$$-\frac{1}{f(t)}i \hbar \frac{df(t)}{dt} = \frac{1}{u(x)} \Bigl[-\frac{\hbar^2}{2m}\nabla^2+V(x)\Bigr] u(x) $$
Since the L.H.S of above Equation is a function of t only and the R.H.S of the Equation is a function of space variables only; the two sides must be equal to a constant. If the constant is tentatively designated as E, two ODEs can be extracted. They are
$$-\frac{1}{f(t)} \frac{df(t)}{dt} = -\frac{iE}{\hbar} \tag{6}$$
$$ \Bigl[-\frac{\hbar^2}{2m}\nabla^2+V(x)\Bigr] u(x)=H(p,x)u(x)=Eu(x)  \tag{7}$$
Solving Equation (6) and omitting the integration constant, we have,
$$ f(t)=exp\frac{-iEt}{\hbar} $$
Equation (7) is the time-independent form of Schrodinger’s Equation for one particle system or atom.  For these cases
$$ |\psi(x,t)|^2 = |u(x)|^2 $$
the probability density does not change with time; hence, these states are called stationary states.
For complex molecules, the time-independent form of Schrodinger’s equation is similar to Equation (7), but includes coordinates for every particle.  It is expressed as


## [Weak Formulation](#weakform_sec)
Weak form of Schrodinger Equation gives

### 1. Time dependent
$$ i\hbar\int \frac{\partial\psi}{\partial t}\delta udv-\int \frac{\hbar^2}{2m} \nabla\psi\cdot\nabla \delta udv+\int \delta uV\psi dv = \oint \frac{\hbar^2}{2m} \frac{d\psi}{dn}\delta uds \tag{7}$$

$$ i\hbar\frac{\partial}{\partial t}\int \phi_i \phi_j dv q_j +\Bigl(-\int \frac{\hbar}{2m} \nabla\phi_j\cdot\nabla\phi_i dv+\int \phi_i V\phi_j dv \Bigr)q_j =  \oint \frac{\hbar}{2m} \frac{d\psi}{dn}\phi_ids \tag{8}$$

### 2. Time independent
$$ -\int \frac{\hbar^2}{2m} \nabla\psi\cdot\nabla \delta udv+\int \delta uV\psi dv = \int E\psi \delta udv+ \oint \frac{\hbar^2}{2m} \frac{d\psi}{dn}\delta uds \tag{9}$$
The problem reads: find a function $\psi$ such that (7) holds for every variation $\delta u$.
We choose a basis $\phi_i$ and substitute $\phi_i$ for $\delta u$ and expand $\psi=\sum q_j\phi_j$
$$ \Bigl(-\int \frac{\hbar}{2m} \nabla\phi_j\cdot\nabla\phi_i dv+\int \phi_i V\phi_j dv \Bigr)q_j = \int E\phi_j\phi_idv+ \oint \frac{\hbar}{2m} \frac{d\psi}{dn}\phi_ids \tag{10}$$
which can be written in a matrix form
$$ \Bigl(K_{ij}+V_{ij}\Bigr)q_j=EM_{ij}q_j+F_i, \tag{11}$$
where
 $$\begin{aligned}
V_{ij}=\int \phi_iV\phi_jdv, \\
M_{ij}=\int \phi_i\phi_jdv, \\
K_{ij}=\frac{\hbar}{2m}\int \nabla\phi_i \cdot \nabla\phi_jdv,  \\
F_i=\frac{\hbar}{2m} \oint\frac{\hbar}{2m}\frac{d\psi}{dn}\phi_ids
\end{aligned}$$
This is a generalized eigenvalue problem, that needs to be solved in our program. For more details, see the section 3.4.

## [The Particle in a 1D Box ](#1DBox_sec)
As a simple example, we will solve the 1D Particle in a Box problem. That is a particle confined to a region $0<x<a$. We can do this with the (unphysical) potential which is zero with in those limits and $+\infty$ outside the limits. 

$$ V(x) = \Biggl ( \begin{matrix}
0 & 0<x<a   \\
\infty & elsewhere
\end{matrix} $$

Because of the infinite potential, this problem has very unusual boundary conditions. (Normally we will require continuity of the wave function and its first derivative.) The wave function must be zero at $x=0$ and $x=a$ since it must be continuous and it is zero in the region of infinite potential. The first derivative does not need to be continuous at the boundary (unlike other problems), because of the infinite discontinuity in the potential.

The time independent Schrödinger equation (also called the energy eigenvalue equation) is 
$$Hu_j=E_ju_j$$
with the Hamiltonian (inside the box)
$$H=-{\hbar^2\over 2m}{d^2\over dx^2}$$
Our solutions will have 
$$u_j=0$$
outside the box.

The solution inside the box could be written as 
$$u_j=e^{ikx}$$

where $k$ can be positive or negative. We do need to choose linear combinations that satisfy the boundary condition that $u_j(x=0)=u_j(x=a)=0$.

We can do this easily by choosing
$$u_j=C\sin(kx)$$
which automatically satisfies the BC at 0. To satisfy the BC at $x=a$ we need the argument of sine to be $n\pi$ there.
$$u_n=C\sin\left({n\pi x\over a}\right)$$
Plugging this back into the Schrödinger equation, we get
$${-\hbar^2\over 2m}(-{n^2\pi^2\over a^2})C\sin(kx)= EC\sin(kx) .$$
There will only be a solution which satisfies the BC for a quantized set of energies. 
$$E_n={n^2\pi^2\hbar^2\over 2ma^2}$$
We have solutions to the Schrödinger equation that satisfy the boundary conditions. Now we need to set the constant $C$ to normalize them to 1. 
$$\langle u_n|u_n\rangle=|C|^2\int\limits_0^a\sin^2\left({n\pi x\over a}\right)dx=|C|^2{a\over 2}$$
Remember that the average value of $\sin^2$ is one half (over half periods). So we set $C$ giving us the eigenfunctions 
$$u_n=\sqrt{2\over a}\sin\left({n\pi x\over a}\right)$$
The first four eigenfunctions are graphed below. The ground state has the least curvature and the fewest zeros of the wavefunction. 
<img src="img/Schrodinger1DBoxResult.png" width=70% alt="hello" title="Particle in a Box Eigenfunctions">
Note that these states would have a definite parity if $x=0$ were at the center of the box. 

The expansion of an arbitrary wave function in these eigenfunctions is essentially our original Fourier Series. This is a good example of the energy eigenfunctions being orthogonal and covering the space.