# Finite Element Analysis of the Shallow Water Equation
   <b>Table of contents </b>
   - <a name="postulates_sec">Navier-Stokes Equation. </a>
   - <a name="timeind_sec">Shallow Water Equation </a>
   - <a name="weak_sed">Weak Form of SWE  </a>

https://en.wikipedia.org/wiki/Derivation_of_the_Navier%E2%80%93Stokes_equations

## [Navier-Stokes Equation](#postulates_sec)
Conservation of Mass:
 $$\frac{\partial \rho}{\partial t}+\nabla \cdot (\rho \mathbf {u})=0 \tag{1}$$
Conservation Of Linear Momentum.
$$\rho {\frac {D\mathbf {u} }{Dt}}=\rho \left( {\frac {\partial \mathbf {u} }{\partial t}+\mathbf {u} \cdot \nabla \mathbf {u} }\right) = \mathbf {s} \quad \Rightarrow  \rho \left( {\frac {\partial \mathbf {u} }{\partial t}}+u{\frac {\partial \mathbf {u} }{\partial x}}+v{\frac {\partial \mathbf {u} }{\partial y}}+w{\frac {\partial \mathbf {u} }{\partial z}} = \mathbf {s} \right)$$
$$ \mathbf  {s} =\nabla \cdot {\boldsymbol  {\sigma }}+{\mathbf  {f}} $$
$$ \boldsymbol  {\sigma } =-p\mathbf {I} +{\boldsymbol {\tau }} $$
$$ \rho {\frac  {D{\mathbf  u}}{Dt}}=-\nabla p+\nabla \cdot {\boldsymbol  \tau }+{\mathbf  {f}} $$

### Compressible Newtonian fluid
$$ \boldsymbol {\tau } =\mu \left(\nabla \mathbf {u} +\left(\nabla \mathbf {u} \right)^{\mathsf {T}}\right)+\lambda \left(\nabla \cdot \mathbf {u} \right)\mathbf {I}  $$

$$ \rho \left({\frac {\partial \mathbf {u} }{\partial t}}+\mathbf {u} \cdot \nabla \mathbf {u} \right)=-\nabla p+\nabla \cdot \left[\mu \left(\nabla \mathbf {u} +\left(\nabla \mathbf {u} \right)^{\mathsf {T}}\right)\right]+\nabla \cdot \left[\lambda \left(\nabla \cdot \mathbf {u} \right)\mathbf {I} \right]+\rho \mathbf {g} $$

## Incompressible Newtonian fluid
$$ \rho \left({\frac {\partial \mathbf {u} }{\partial t}}+\mathbf {u} \cdot \nabla \mathbf {u} \right)=-\nabla p+\nabla \cdot \left[\mu \left(\nabla \mathbf {u} +\left(\nabla \mathbf {u} \right)^{\mathsf {T}}\right)\right]+\rho \mathbf {g}  $$

$$ \nabla \cdot \sigma = -\nabla p + \mu\nabla^2 u = -\nabla p + \rho \nu\nabla^2 u$$

$$\frac{\partial}{\partial t}(\rho v)+\nabla \cdot (\rho vv)-\rho b -\nabla \cdot T=0 \tag{2}$$
For a Newtonian fluid, $T=-pI+T^{'}$, Equation (2) becomes
$$\frac{\partial}{\partial t}(\rho v)+\nabla \cdot (\rho vv)=-\nabla p+\rho b+\nabla \cdot T^{'}$$
if fluid density $\rho$ be a constant, equation (1) becomes
$$\nabla \cdot v=0$$

## [Shallow Water Equation](#timeind_sec)

<img src="img/watercolumn.PNG" width=50% alt="hello" title="Shallow water">

### Boundary condition
- At the bottom (z = −b)
  - No slip $ u=v=0 $
  - No normal flow: 
  $$ u\frac{\partial b}{\partial x}+v\frac{\partial b}{\partial y} + w = 0 $$
  - Bottom shear stress: 
   $$\tau_{bx}=\tau_{xx}\frac{\partial b}{\partial x}+ \tau_{xy}\frac{\partial b}{\partial y}+\tau_{xz}$$
  where $\tau_{bx}$ is specified bottom friction (similarly for y direction).
- At the free surface (z = ζ)
  - No relative normal flow:
   $$ \frac{\partial \eta}{\partial t}+ u\frac{\partial \eta}{\partial x}+ v\frac{\partial \eta}{\partial y} - w = 0 $$
  - p=0
  - Surface shear stress:
$$\tau_{sx}=\tau_{xx}\frac{\partial \eta}{\partial x}+ \tau_{xy}\frac{\partial \eta}{\partial y}+\tau_{xz}$$

### z-momentum Equation
Before we integrate over depth, we can examine the momentum equation for vertical velocity. By a scaling argument, all of the terms except the pressure derivative and the gravity term are small. Then the z-momentum equation collapses to
$$\frac{\partial p}{\partial z}=\rho g$$
implying that $p=\rho g(\eta-z)$
This is the hydrostatic pressure distribution. Then
$$\frac{\partial p}{\partial x}=\rho g\frac{\partial \eta}{\partial x}; \frac{\partial p}{\partial y}=\rho g\frac{\partial \eta}{\partial y}$$


### Continuity Equation
We now integrate the continuity equation ∇ · v = 0 from z = −b to z = ζ. Since both b and ζ depend on t, x, and y, we apply the Leibniz integral rule:

$$ 0 = \int_{-b}^\eta \nabla \cdot vdz=\int_{-b}^\eta \Bigl( \frac{\partial u}{\partial x}+\frac{\partial u}{\partial y}\Bigr )dz +w|_{z=\eta} - w|_{z=-b} = \frac{\partial}{\partial x}\int_{-b}^\eta udz + \frac{\partial}{\partial y}\int_{-b}^\eta udz - \Bigl (u|_{z=\eta}\frac{\partial \eta}{\partial x}+ u|_{z=-b}\frac{\partial b}{\partial x} \Bigr ) - \Bigl (v|_{z=\eta}\frac{\partial \eta}{\partial y}+ v|_{z=-b}\frac{\partial b}{\partial y} \Bigr )+w|_{z=\eta} - w|_{z=-b}$$

Defining depth-averaged velocities as
$$\bar{u}=\frac{1}{H}\int_{-b}^\eta udz; \bar{v}=\frac{1}{H}\int_{-b}^\eta vdz$$
we can use our BCs to get rid of the boundary terms. So the depth-averaged continuity equation is

$$ \frac{\partial H}{\partial t} + \frac{\partial}{\partial x}(H\bar{u}) + \frac{\partial}{\partial y}(H\bar{v})=0 \tag{9}$$

### LHS of the x- and y-Momentum Equations

If we integrate the left-hand side of the x-momentum equation over depth, we get:

$$\int_{-b}^\eta \Bigl [\frac{\partial}{\partial t}u + \frac{\partial}{\partial x}u^2 + \frac{\partial}{\partial y}(uv)+ \frac{\partial}{\partial z}(uw) \Bigr ]= \frac{\partial}{\partial t}(H\bar{u}) + \frac{\partial}{\partial x}(H\bar{u}^2) + \frac{\partial}{\partial y}(H\bar{u}\bar{v})+ {differential \quad advection \quad terms}$$
The differential advection terms account for the fact that the average of the product of two functions is not the product of the averages. We get a similar result for the left-hand side of the y-momentum equation.

### RHS of x- and y-Momentum Equations

Integrating over depth gives us
$$ \Biggl ( \begin{matrix}
-\rho gH\frac{\partial \zeta}{\partial x} + \tau_{sx} - \tau_{bx} + \frac{\partial}{\partial x}\int_{-b}^{\zeta}\tau_{xx} + \frac{\partial}{\partial y}\int_{-b}^{\zeta}\tau_{xy} \\
-\rho gH\frac{\partial \zeta}{\partial y} + \tau_{sy} - \tau_{by} + \frac{\partial}{\partial y}\int_{-b}^{\zeta}\tau_{xy} + \frac{\partial}{\partial y}\int_{-b}^{\zeta}\tau_{yy}
\end{matrix} $$


Combining the depth-integrated continuity equation with the LHS and RHS of the depth-integrated x- and y-momentum equations, the 2D (nonlinear) SWE in conservative form are:

$$ \Biggl [ \begin{matrix}
\frac{\partial H}{\partial t} + \frac{\partial}{\partial x}(H\bar{u}) + \frac{\partial}{\partial y}(H\bar{v})=0 \\
\frac{\partial H\bar{u}}{\partial t} + \frac{\partial}{\partial x}(H\bar{u}^2) + \frac{\partial}{\partial y}(H\bar{u}\bar{v})=-gH\frac{\partial \zeta}{\partial x}+\frac{1}{\rho}[\tau_{sx} - \tau_{bx}+F_x] \\
\frac{\partial H\bar{v}}{\partial t} + \frac{\partial}{\partial x}(H\bar{u}\bar{v}) + \frac{\partial}{\partial y}(H\bar{v}^2)=-gH\frac{\partial \zeta}{\partial y}+\frac{1}{\rho}[\tau_{sy} - \tau_{by}+F_y]
\end{matrix} $$

The surface stress, bottom friction, and Fx and Fy must still determined on a case-by-case basis

$$ \partial_t \left (\begin{matrix} H \\ H\bar{u} \\ H\bar{v} \end{matrix} \right )+ \partial_x \left (\begin{matrix} Hu \\ gH^2/2+Hu^2 \\ Huv \end{matrix} \right ) + \partial_y \left (\begin{matrix} Hv \\ Huv \\ gH^2/2+Hv^2 \end{matrix} \right ) = \left( \begin{matrix} 0\\\frac{1}{\rho}[\tau_{sx} - \tau_{bx}+F_x]\\ \frac{1}{\rho}[\tau_{sy} - \tau_{by}+F_y] \end{matrix} \right) $$

## [Weak Form of SWE ](#weak_sec)
The standard Galerkin semidiscretization of (2.1) is defined as follows: 
$$ \Bigg \langle \frac{\partial H}{\partial t}, \delta H\Bigg \rangle - \Bigg \langle Hu, \frac{\partial \delta H}{\partial x}\Bigg \rangle - \Bigg \langle Hv, \frac{\partial \delta H}{\partial y}\Bigg \rangle =0 $$

$$ V(x) = \Biggl ( \begin{matrix}
0 & 0<x<a   \\
\infty & elsewhere
\end{matrix} $$

Because of the infinite potential, this problem has very unusual boundary conditions. (Normally we will require continuity of the wave function and its first derivative.) The wave function must be zero at $x=0$ and $x=a$ since it must be continuous and it is zero in the region of infinite potential. The first derivative does not need to be continuous at the boundary (unlike other problems), because of the infinite discontinuity in the potential.

The time independent Schrödinger equation (also called the energy eigenvalue equation) is 
$$Hu_j=E_ju_j$$
with the Hamiltonian (inside the box)
$$H=-{\hbar^2\over 2m}{d^2\over dx^2}$$
Our solutions will have 
$$u_j=0$$
outside the box.

The solution inside the box could be written as 
$$u_j=e^{ikx}$$

where $k$ can be positive or negative. We do need to choose linear combinations that satisfy the boundary condition that $u_j(x=0)=u_j(x=a)=0$.

We can do this easily by choosing
$$u_j=C\sin(kx)$$
which automatically satisfies the BC at 0. To satisfy the BC at $x=a$ we need the argument of sine to be $n\pi$ there.
$$u_n=C\sin\left({n\pi x\over a}\right)$$
Plugging this back into the Schrödinger equation, we get
$${-\hbar^2\over 2m}(-{n^2\pi^2\over a^2})C\sin(kx)= EC\sin(kx) .$$
There will only be a solution which satisfies the BC for a quantized set of energies. 
$$E_n={n^2\pi^2\hbar^2\over 2ma^2}$$
We have solutions to the Schrödinger equation that satisfy the boundary conditions. Now we need to set the constant $C$ to normalize them to 1. 
$$\langle u_n|u_n\rangle=|C|^2\int\limits_0^a\sin^2\left({n\pi x\over a}\right)dx=|C|^2{a\over 2}$$
Remember that the average value of $\sin^2$ is one half (over half periods). So we set $C$ giving us the eigenfunctions 
$$u_n=\sqrt{2\over a}\sin\left({n\pi x\over a}\right)$$
The first four eigenfunctions are graphed below. The ground state has the least curvature and the fewest zeros of the wavefunction. 
<img src="img/Schrodinger1DBoxResult.png" width=70% alt="hello" title="Particle in a Box Eigenfunctions">
Note that these states would have a definite parity if $x=0$ were at the center of the box. 

The expansion of an arbitrary wave function in these eigenfunctions is essentially our original Fourier Series. This is a good example of the energy eigenfunctions being orthogonal and covering the space.