# Finite Element Analysis of the Shallow Water Equation
   <b>Table of contents </b>
   - <a name="postulates_sec">Navier-Stokes Equation. </a>
   - <a name="timeind_sec">Shallow Water Equation </a>
   - <a name="weak_sed">Weak Form of SWE  </a>

## [Navier-Stokes Equation](#postulates_sec)
Conservation of Mass:
 $$\frac{\partial \rho}{\partial t}+\nabla \cdot (\rho v)=0 \tag{1}$$
Conservation Of Linear Momentum.
$$\frac{D}{D t}(\rho v)-\rho b -\nabla \cdot T=0 \tag{2}$$
For a Newtonian fluid, $T=-pI+T^{'}$, Equation (2) becomes
$$\frac{D}{Dt}(\rho v)=-\nabla p+\rho b+\nabla \cdot T^{'}$$
if density $\rho$ be a constant, equation (1) (2) becomes
$$\nabla \cdot v=0 \tag{3}$$
$$\frac{Dv}{Dt}=-\frac{1}{\rho}(\nabla p+\nabla \cdot T^{'})+b  \tag{4}$$

If $T'=\rho\nu\nabla v$
$$\frac{Dv}{Dt}=-\frac{1}{\rho}\nabla p+\nu \Delta v+b $$
or
$$\frac{\partial v}{\partial t} +(v \cdot\nabla)v=-\frac{1}{\rho}\nabla p+\nu \Delta v+b$$
when consider Coriolis force
$$ b=f_c \mathbf {k} \times \mathbf {u} $$
where k is a unit vector in the vertical direction, $f_c$ is a Coriolis function.

## [Shallow Water Equation](#timeind_sec)

<img src="img/watercolumn.PNG" width=50% alt="hello" title="Shallow water">

In z direction
$$ p=p_s+ \rho g(\zeta-z)$$

$$\frac{1}{\rho}\nabla p= g \nabla \zeta$$ 

where $ \nabla= (\partial_x, \partial_y) $ because $\partial_z=0$. Equation (3) then becomes

$$\frac{Dv}{Dt}=-g\nabla \zeta+\frac{1}{\rho}\nabla \cdot T^{'} \tag{4}$$

Defining is the depth-averaged horizontal velocity U, $H=\zeta +b, \zeta = H-b$

$$ \mathbf{U}=\frac{1}{\zeta +b}\int_{-b}^h \mathbf{u}dz $$

This wave model consists of solving a generalized wave continuity equation for the free surface elevation together with the conservative momentum equations to solve for velocities. In this model, the generalized wave continuity equation replaces the continuity equation (3)

$$ \frac{\partial \zeta}{\partial t} + \nabla \cdot (\mathbf{u}H) = 0 \tag{5} $$


If we integrate the left-hand side of the x-momentum equation over depth, we get:

$$\int_{-b}^\eta \Bigl [\frac{\partial}{\partial t}u + \frac{\partial}{\partial x}u^2 + \frac{\partial}{\partial y}(uv)+ \frac{\partial}{\partial z}(uw) \Bigr ]= \frac{\partial}{\partial t}(H\bar{u}) + \frac{\partial}{\partial x}(H\bar{u}^2) + \frac{\partial}{\partial y}(H\bar{u}\bar{v})$$
The differential advection terms account for the fact that the average of the product of two functions is not the product of the averages. We get a similar result for the left-hand side of the y-momentum equation.

$$ {\frac {\partial H\mathbf {U} }{\partial t}+H\mathbf {U} \cdot \nabla \mathbf {U} } = -gH\nabla \zeta+ \frac{1}{\rho}H\nabla \cdot {\boldsymbol  \tau }+\frac{1}{\rho}{\mathbf{f}} \tag{6} $$


The vector f represents the integration of $\tau_{zx}$ and $\tau_{zy}$ over the vertical direction and if the difference betwwen surface and bottom shear stresses, defined as

$$ \mathsf {f} = \Biggl \{ \begin{matrix}
(\tau_{zx})_s -  (\tau_{zx})_b \\
(\tau_{zy})_s -  (\tau_{zy})_b
\end{matrix} $$

Employing the continuity equation (5), the equations may be simplified to 

$$ \frac {\partial HU }{\partial t}+\frac {\partial HU^2 }{\partial x} +  \frac {\partial HUV }{\partial y} = H\frac {\partial U }{\partial t}+HU\frac {\partial U }{\partial x} +  HV \frac {\partial U}{\partial y} + U(\frac {\partial H }{\partial t}+\frac {\partial UH }{\partial x} +  \frac {\partial VH}{\partial y}) = H\frac {\partial U }{\partial t}+HU\frac {\partial U }{\partial x} +  HV \frac {\partial U}{\partial y} $$

$$ \frac {\partial \mathbf {U} }{\partial t}+\mathbf {U} \cdot \nabla \mathbf {U} = -g\nabla \zeta - \frac{1}{\rho}\nabla \cdot {\boldsymbol  \tau }+\frac{1}{H\rho}{\mathbf{f}}  \tag{7} $$

Equation (6) and (7) are same equation with different form.

### Boundary condition
 In the SWE solutions, the types of boundary can be inflow, outflow, solid, symmetry and periodic


- At the bottom (z = −b)
  - No slip $ u=v=0 $
  - No normal flow: 
  $$ u\frac{\partial b}{\partial x}+v\frac{\partial b}{\partial y} + w = 0 $$
  - Bottom shear stress: 
   $$\tau_{bx}=\tau_{xx}\frac{\partial b}{\partial x}+ \tau_{xy}\frac{\partial b}{\partial y}+\tau_{xz}$$
  where $\tau_{bx}$ is specified bottom friction (similarly for y direction).
- At the free surface (z = ζ)
  - No relative normal flow:
   $$ \frac{\partial \eta}{\partial t}+ u\frac{\partial \eta}{\partial x}+ v\frac{\partial \eta}{\partial y} - w = 0 $$
  - p=0
  - Surface shear stress:
$$\tau_{sx}=\tau_{xx}\frac{\partial \eta}{\partial x}+ \tau_{xy}\frac{\partial \eta}{\partial y}+\tau_{xz}$$

### Matrix form of equation (6)
Combining the depth-integrated continuity equation with the LHS and RHS of the depth-integrated x- and y-momentum equations, the 2D (nonlinear) SWE in conservative form are:

$$ \Biggl [ \begin{matrix}
\frac{\partial H}{\partial t} + \frac{\partial}{\partial x}(H\bar{u}) + \frac{\partial}{\partial y}(H\bar{v})=0 \\
\frac{\partial H\bar{u}}{\partial t} + \frac{\partial}{\partial x}(H\bar{u}^2) + \frac{\partial}{\partial y}(H\bar{u}\bar{v})=-gH\frac{\partial \zeta}{\partial x}+\frac{1}{\rho}[\tau_{sx} - \tau_{bx}+F_x] \\
\frac{\partial H\bar{v}}{\partial t} + \frac{\partial}{\partial x}(H\bar{u}\bar{v}) + \frac{\partial}{\partial y}(H\bar{v}^2)=-gH\frac{\partial \zeta}{\partial y}+\frac{1}{\rho}[\tau_{sy} - \tau_{by}+F_y]
\end{matrix} \tag{8} $$

The surface stress, bottom friction, and Fx and Fy must still determined on a case-by-case basis

$$ \partial_t \left (\begin{matrix} H \\ H\bar{u} \\ H\bar{v} \end{matrix} \right )+ \partial_x \left (\begin{matrix} Hu \\ gH^2/2+Hu^2 \\ Huv \end{matrix} \right ) + \partial_y \left (\begin{matrix} Hv \\ Huv \\ gH^2/2+Hv^2 \end{matrix} \right ) = \left( \begin{matrix} 0\\ -gH\frac{\partial b}{\partial x}+ \frac{1}{\rho}[\tau_{sx} - \tau_{bx}+F_x]\\ -gH\frac{\partial b}{\partial y}+\frac{1}{\rho}[\tau_{sy} - \tau_{by}+F_y] \end{matrix} \right) $$

In vector form
$$\frac{\partial \bold{u}}{\partial t} + div\bold{F(u)}=\bold{r}$$
where
$$\bold{u}=\left ( \begin{matrix} H \\ H\bar{u} \\ H\bar{v} \end{matrix} \right )$$
$$\bold{F(u)}=\left ( \begin{matrix} H\bold{v} \\ Hv_x\bold{v}+1/2gH^2\bold{e}_x \\ Hv_y\bold{v}+1/2gH^2\bold{e}_y \end{matrix}  \right )$$
$$\bold{r}=\left ( \begin{matrix} 0 \\ -gH\partial_xb \\ -gH\partial_yb \end{matrix} \right )$$

the mathematical meaning of div in SWE context is:
$$div=(\nabla\cdot,\nabla\cdot,\nabla\cdot), \nabla=\left( \frac{\partial}{\partial x},\frac{\partial}{\partial y} \right) $$

### Matrix form of equation (7)

$$ \frac{\partial \zeta}{\partial t} + \frac{\partial ([\zeta+b]U)}{\partial x} + \frac{\partial ([\zeta+b]V)}{\partial y} = 0 \tag{5} $$

$$ \partial_t \left (\begin{matrix} \zeta \\ U \\ V \end{matrix} \right )+ \left (\begin{matrix} U & H & 0\\ g & U & 0 \\ 0 & 0 &U \end{matrix} \right ) \partial_x \left (\begin{matrix} \zeta \\ U \\ V \end{matrix} \right ) + \left (\begin{matrix} V & 0 & H\\ 0 & V & 0 \\ g & 0 &V \end{matrix} \right ) \partial_y \left (\begin{matrix} \zeta \\ U \\ V \end{matrix} \right ) = \left( \begin{matrix} U\frac{\partial b}{\partial x}+ V\frac{\partial b}{\partial y} \\ \frac{1}{\rho}[\tau_{sx} - \tau_{bx}+F_x]\\ \frac{1}{\rho}[\tau_{sy} - \tau_{by}+F_y] \end{matrix} \right) $$


## [Weak Form of SWE ](#weak_sec)

$$ \int_\Omega \frac{\partial \mathbf {u}}{\partial t}\phi dxdy + \int_{\partial\Omega} \bold{F}_j\cdot \bold{n}\phi dS - \int_{\Omega} \bold{F}_j\cdot \nabla\phi dxdy=\int_{\Omega} \bold{r}\phi dxdy $$

For continuity equation in equation (8)
$$ \int_\Omega \frac{\partial H}{\partial t} \phi dxdy + \int_{\Omega} HU \frac{\partial \phi}{\partial x} dxdy + \int_{\Omega}HV \frac{\partial \phi}{\partial y} dxdy= \int_{\Omega} \frac{\partial HU \phi}{\partial x} dxdy + \int_{\Omega}\frac{\partial HV\phi}{\partial y} dxdy = \int(HU\phi)|_0^L dy + \int(HV\phi)|_0^D dx = \int_{\partial\Omega} (HU, HV) \left (\begin{matrix} n_1 \\ n_2  \end{matrix} \right )\phi dS$$

For momentum equation in equation (8)



And

$$ \int_\Omega \partial_t \left (\begin{matrix} \zeta \\ U \\ V \end{matrix} \right )\phi dxdy + \int_{\partial\Omega} \left (\begin{matrix} U & H & 0\\ g & U & 0 \\ 0 & 0 &U \end{matrix} \right )  \left (\begin{matrix} \zeta \\ U \\ V \end{matrix} \right ) \bold{n}\phi dS - \int_{\Omega} \left (\begin{matrix} U & H & 0\\ g & U & 0 \\ 0 & 0 &U \end{matrix} \right )  \left (\begin{matrix} \zeta \\ U \\ V \end{matrix} \right ) \nabla\phi dxdy=\int_{\Omega} \bold{r}\phi dxdy $$
