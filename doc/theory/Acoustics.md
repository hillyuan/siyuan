# Finite Element Analysis of the Acoustics Equation
   <b>Table of contents </b>
   - <a name="postulates_sec"> Equation of acoustic propagation. </a>
   - <a name="timeind_sec">Solution of equation of acoustic propagation </a>
   - <a name="weak_sed">Variational form of Helmholtz equation  </a>
   - <a name="weak_time">Variational form of Time-domain equation  </a>

## [ Equation of acoustic propagation](#postulates_sec)
Acousticc equation consider non-viscous fluid with irrotational valocity. It is thus could derived from general Navier-Stokes equations

Conservation of Mass:
 $$\frac{\partial \rho}{\partial t}+\nabla \cdot (\rho v)=q \tag{1}$$
Conservation of Linear Momentum.
$$\frac{D}{Dt}(\rho v)+\nabla p-\nabla \cdot \tau=f  \tag{2}$$

with specific constitutive relations
$$\frac{Dp}{Dt}=c^2\frac{D\rho}{Dt}  \tag{3}$$
where c is speed of sound, considered.

We set the visous fluid tensor $\tau$ to zero, neglect external force density f, and write velocity v by a scalar potential via
  $$ v=\nabla \phi \tag{4}$$
brcause it is irrotational.

It is therefore from equation(1) that
$$\frac{\partial \rho}{\partial t}+\nabla \cdot (\rho v)=\frac{\partial \rho}{\partial t}+v \cdot \nabla \rho + \rho\nabla \cdot v=\frac{D\rho}{Dt}+ \rho\nabla \cdot \nabla \phi = 0 $$
or
$$ \frac{D\rho}{Dt} = - \rho \nabla \cdot v = - \rho \nabla \cdot \nabla \phi $$
and
$$ \frac{1}{c^2} \frac{Dp}{Dt} = - \rho \nabla \cdot v = -\rho \nabla \cdot \nabla \phi \tag{5} $$
after introducing equation (3).

Equation (2) now simplified as 

$$ \frac{D}{Dt}(\rho v)+\nabla p =q \tag{6} $$

- In $\phi$ form

Now, we apply ∇ to (5), D/Dt to (6) and by subtract the resulting equations we arrive at

$$ \frac{D^2}{Dt^2}(\rho v) - \nabla \big (\rho c^2\nabla \cdot v \big )=\frac{Dq}{Dt} $$

or
$$ \nabla \bigg [  \frac{D^2}{Dt^2}( \rho \phi)-\rho c^2 \nabla \cdot \nabla \phi \bigg]=0 $$

This equation is clearly satisfied, when $\phi$ fulfills
$$ \frac{D^2}{Dt^2}( \rho\phi)-\rho c^2 \nabla \cdot \nabla \phi =0 \tag{7}$$

we consider
$$ p=p_0+p_a;  \rho = \rho_0+\rho_a $$

where $p_0$ and $\rho_0$ are background quantities of the medium being at rest, $p_a$ and $\rho_a$ are contribution due to acoustics. we suppose
$$ p_a << p_0; \rho_a << \rho_0$$

Equation (6) then turns into
$$ \rho_0\frac{D^2\phi}{Dt^2}-\rho_0 c^2 \nabla \cdot \nabla \phi =\rho_a c^2 \nabla \cdot \nabla \phi - \frac{D^2}{Dt^2}( \rho_a\phi) \approx 0 $$

or 
$$ \frac{D^2\phi}{Dt^2}- c^2 \nabla \cdot \nabla \phi = 0 \tag{8} $$

Which is the basic equation to be solved in acoustic calculation. After scalar potential $\phi$ be obtained. Velocity v coule be ontained by equation (4), and, from equation (6), 
$$ \rho_0\frac{D}{Dt}(\nabla \phi)+\nabla p =\nabla \left( \rho_0 \frac{D\phi}{Dt} +p \right) = 0 $$
from which $p=-\rho_0 \frac{D\phi}{Dt}$ could be used to calculate pressure p. 

- In p form

Applying ∇ to (6), D/Dt to (5) and by subtract the resulting equations we arrive at
$$ \frac{1}{c^2}\frac{D^2p}{Dt^2} -\frac{D}{Dt}(v \nabla\rho) - \nabla \cdot \nabla p=\frac{Dq}{Dt} -\nabla f $$

The second term of left side 
$$\frac{D}{Dt}(v \nabla\rho) = \frac{D}{Dt}\left((v_0+v_a) \nabla(\rho_0+\rho_a)\right) = \frac{D}{Dt}\left(v_a\nabla(\rho_0+\rho_a)\right) = \frac{D}{Dt}\left(v_a\nabla \rho_a\right) \approx 0 $$
The equation then becomes
$$ \frac{1}{c^2}\frac{D^2p}{Dt^2}  - \nabla \cdot \nabla p=\frac{Dq}{Dt} -\nabla f \tag{9} $$
which shares the same form with equation (8).

By performing a Fourier transform upon equation (9), we arrive at Helmholtz equation 

$$ \nabla^2 P + k^2P = -j\omega Q \tag{10}$$

where $k=\frac{\omega}{c}=2\pi\frac{f}{c} $ is the acoustic wavenumber,

## [Solution of acoustics equation](#timeind_sec)

 - ### Eigen value  problem
 $$ \nabla^2 P + k^2P = 0 \tag{11} $$
 - ### Direct solution of Helmholtz equation (8) or (10)
 - ### Time-domian solution of equation (9)
 - ### Frequency-domain solution of equation (9)


## [Variational form of Helmholtz equation ](#weak_sec)
Consider following boundary value proeble
$$ \nabla^2 P + k^2P = -j\omega Q \quad on \ \Omega\tag{12} \\
\frac{\partial P}{\partial n} + \beta P=g \quad on \ \partial\Omega $$

Multiplying (12) by a test function $V \isin H^1(\Omega)$ and integrating we get  

$$ \int_\Omega \nabla P(\mathbf{x}, \omega) \cdot \nabla V(\mathbf{x}, \omega) \mathrm{d}\Omega  - \int_{\partial \Omega} V(\mathbf{x}, \omega) \frac{\partial}{\partial n}  P(\mathbf{x}, \omega) \mathrm{d}S - \frac{\omega^2}{c^2} \int_\Omega P(\mathbf{x}, \omega) V(\mathbf{x}, \omega) \mathrm{d}\Omega = \int_\Omega Q(\mathbf{x}, \omega) V(\mathbf{x}, \omega) \mathrm{d}\Omega
$$

and introducing the Robin boundary condition into the second integral yields

$$ \int_\Omega \nabla P(\mathbf{x}, \omega) \cdot \nabla V(\mathbf{x}, \omega) \mathrm{d}\Omega  + \beta \int_{\partial \Omega} V(\mathbf{x}, \omega) P(\mathbf{x}, \omega) \mathrm{d}S - \frac{\omega^2}{c^2} \int_\Omega P(\mathbf{x}, \omega) V(\mathbf{x}, \omega) \mathrm{d}\Omega \\ = \int_\Omega Q(\mathbf{x}, \omega) V(\mathbf{x}, \omega) \mathrm{d}\Omega + \int_{\partial \Omega} V(\mathbf{x}, \omega)g dS .  \tag{13}$$

which is the governing equation of FEM analysis.

### Boundary conditions

A useful quantity in acoustics is impedance, which is a measure of the amount by which the motion induced by a pressure applied to a surface is impeded. However, a quantity that varies with time and depends on initial values is not of interest. Thus the impedance is defined via the Fourier transform by
$$ Z(\mathbf{x}, \omega) = \frac{\mathbf{p}(\mathbf{x}, \omega)}{\mathbf{v(\mathbf{x}, \omega)} \cdot \mathbf{n(\mathbf{x})}} \\
V_n(\mathbf{x}, \omega) + \frac{1}{Z(\mathbf{x}, \omega)} P(\mathbf{x}, \omega) = 0 \qquad  \tag{14}$$
at a point $\mathbf{x}$ on the surface $\partial \Omega$ with unit normal vector $\mathbf{n}$. It is in general a complex number and its real part is called resistance, its imaginary part reactance and its inverse the admittance denoted by $Y(\mathbf{x}, \omega)$.

For realistic simulations, a good approximation of the actual physical boundary conditions is essential. In the two simple cases - acoustically hard and soft boundary the solution is easy:

- Acoustically hard boundary: Here, the reflection coefficient R gets 1 (total reflection), which means that the surface impedance has to approach infinity. According to (14), the term n · va has to be zero. Using the linearized momentum equation (6) with zero source term, we arrive at the Neumann boundary condition
$$ n \cdot \nabla p = \frac{\partial p}{\partial n}=0$$

- Acoustically soft boundary: In this case, the acoustic impedance gets zero, which simply results in a homogeneous Dirichlet boundary condition
$$ p =0 $$

- Since real surfaces (boundaries) are never totally hard or totally soft, it seems to be a good idea to use a Robin boundary condition as a model
$$ \frac{\partial p}{\partial n} + \beta p=0 \\$$
In the time harmonic case, we can explore (6) with zero source term and apply a dot product with the normal vector n
$$ jρ_0ωn · v + \frac{∂ p}{∂n} = 0 \\
 -\mathrm{j} \omega \rho_0 V_n(\mathbf{x}, \omega) = \frac{\partial}{\partial n} P(\mathbf{x}, \omega) ,  \qquad \text{for } x \in \partial \Omega_v $$
If Z value is provided, by using (14) we obtain
$$ \frac{\partial}{\partial n} P(\mathbf{x}, \omega) + \mathrm{j} \underbrace{\frac{\omega \rho_0}{Z}}_{\sigma} P(\mathbf{x}, \omega) = 0 \qquad \text{for } x \in \partial \Omega_z . $$
The medium impedance of air for free-field propagation is $𝑍_0=\rho_0 𝑐$ , hence $ \sigma_0 = \frac{\omega}{c}$ in this case. Free-field conditions can be simulated by matching the impedance of the boundary to $Z_0$ .

As known from measurements, Z is a function of frequency and therefore a inverse Fourier transform to arrive at a time domain formulation results in a convolution integral. Furthermore, Z depends on the incident angle of the acoustic wave, which makes acoustic computations of rooms quite complicated. Therefore, often the computational domain is not limited by an impedance boundary condition, but the surrounded elastic body is taken into account.


It is common to express this integral equation in terms of the bilinear $𝑎(𝑃,𝑉)$ and linear $𝐿(𝑉)$ forms

$$ a(P, V) = \frac{\omega^2}{c^2} \int_\Omega P(\mathbf{x}, \omega) V(\mathbf{x}, \omega) \mathrm{d}\Omega - \int_\Omega \nabla P(\mathbf{x}, \omega) \cdot \nabla V(\mathbf{x}, \omega) \mathrm{d}\Omega + \mathrm{j} \sigma \int_{\partial \Omega_v} V(\mathbf{x}, \omega) P(\mathbf{x}, \omega) \mathrm{d}S ,\\ L(V) = -\int_\Omega Q(\mathbf{x}, \omega) V(\mathbf{x}, \omega) \mathrm{d}\Omega - j\omega \rho_0\int_{\partial \Omega_v} V(\mathbf{x}, \omega) V_n(\mathbf{x}, \omega) \mathrm{d}S , $$

where
$$ a(P, V) = L(V) $$

Computational implementations of the FEM (like FEniCS) may not be able to handle complex numbers. In this case the problem can be split into two coupled problems with respect to the real and imaginary part. By introducing $P(\mathbf{x}, \omega) = P_r(\mathbf{x}, \omega) + \mathrm{j} P_i(\mathbf{x}, \omega)$ and $Q(\mathbf{x}, \omega) = Q_r(\mathbf{x}, \omega) + \mathrm{j} Q_i(\mathbf{x}, \omega)$,

$$ a(P, V) = V(\mathbf{x}, \omega)\left( \frac{\omega^2}{c^2} \int_\Omega P(\mathbf{x}, \omega) N \mathrm{d}x - \int_\Omega \nabla P(\mathbf{x}, \omega) \cdot \nabla N \mathrm{d}x + \mathrm{j} \sigma \int_{\partial \Omega_v} P(\mathbf{x}, \omega) N \mathrm{d}S \right),\\ L(V) = V(\mathbf{x}, \omega) \left(-\int_\Omega Q(\mathbf{x}, \omega) N \mathrm{d}\Omega - j\omega \rho_0\int_{\partial \Omega_v}  V_n(\mathbf{x}, \omega) N \mathrm{d}S \right), $$

 and identifying the real and imaginary parts of the bilinear and linear forms, we get

$$ a_r = \int_V \left( \frac{\omega^2}{c^2} N P_r(\mathbf{x}, \omega) - \nabla P_r(\mathbf{x}, \omega) \cdot \nabla N(\mathbf{x}, \omega) +\right) \mathrm{d}\Omega - \\
\sigma \cdot \int_{\partial V} \left( N(\mathbf{x}, \omega) P_i(\mathbf{x}, \omega) \right) \mathrm{d}S, $$

$$ a_i = \int_V \left( \frac{\omega^2}{c^2} N(\mathbf{x}, \omega) P_i(\mathbf{x}, \omega) - \nabla P_i(\mathbf{x}, \omega) \cdot \nabla N(\mathbf{x}, \omega) \right) \mathrm{d}\Omega + \\
\sigma \cdot \int_{\partial V} \left( N(\mathbf{x}, \omega) P_r(\mathbf{x}, \omega) \right) \mathrm{d}S $$

for the bilinear form.

In order to uniquely define the pressure field in domain V, one boundary condition must be specified at each position on the closed boundary surface

- imposed presusure
$$ p=\bar{p}␣ on ␣\Omega_p $$
- imnposed normal velocity
$$ v_n=\frac{j}{\rho\omega}\frac{\partial p}{\partial n} = \bar{v}_n␣ on ␣\Omega_p $$
- imposed normal impedance
$$ p=\bar{Z} v_n $$

## [Variational form of Time-domain equation ](#weak_time)
Weak form of equation (9)

$$ \int_\Omega \frac{1}{\rho_0 c^2} W \frac{\partial^2 P}{\partial t^2} \mathrm{d}\Omega + \int_\Omega \frac{1}{\rho_0}\nabla P(\mathbf{x}, \omega) \cdot \nabla W(\mathbf{x}, \omega) \mathrm{d}\Omega = \\ \int_\Omega \frac{1}{\rho_0}\frac{\partial Q(\mathbf{x}, \omega)}{\partial t} W(\mathbf{x}, \omega) \mathrm{d}\Omega + \int_{\partial \Omega} W(\mathbf{x}, \omega) \frac{1}{\rho_0}n\nabla P dS .  \tag{15}$$

From the equation of momentum conservation, the normal velocity on the 
boundary of the acoustic domain is given by:

$$\frac{\partial v_n}{\partial t}=n \frac{\partial v}{\partial t} = -\frac{1}{\rho_0}n\nabla p,  \quad n \frac{\partial v}{\partial t} = \frac{\dot p}{Z}$$

Equation (15) could be rewritten as

$$ \int_\Omega \frac{1}{\rho_0 c^2} W \frac{\partial^2 P}{\partial t^2} \mathrm{d}\Omega + \int_\Omega \frac{1}{Z}\dot P(\mathbf{x}, \omega) \cdot W(\mathbf{x}, \omega) \mathrm{d}\Omega + \int_\Omega \frac{1}{\rho_0}\nabla P(\mathbf{x}, \omega) \cdot \nabla W(\mathbf{x}, \omega) \mathrm{d}\Omega = \\ \int_\Omega \frac{1}{\rho_0}\frac{\partial Q(\mathbf{x}, \omega)}{\partial t} W(\mathbf{x}, \omega) \mathrm{d}\Omega + \int_{\partial \Omega_p} W(\mathbf{x}, \omega) \frac{1}{\rho_0}n\nabla P dS - \int_{\partial \Omega_a} W(\mathbf{x}, \omega) a dS .$$

to consider acceleration boundary condition.

## Reference
- Simens: Acoustics User’s Guide,
- Nikolaos M. Papadakis: Application of Finite Element Method for Estimation of Acoustic Parameters, 2017