
# Finite Element Analysis of the Grad-Shafranov Equation

## Formulation

The grad-shafranov equation is derived from the magnetohydrodynamic (MHD) equilibrium equations given by

The relevant static magnetohydrodynamic equations are the balance between
the pressure force and the Lorentz force
$$ J \times B=\nabla P  $$
Where, 𝐽 is the current density, 𝐵 is the magnetic field and 𝑃 is the pressure

Introduce Maxwell equation $  \mu_0 J = \nabla \times B $ and use vector potential A to indicate $ B= \nabla \times A $. The above equations becomes
$$ \nabla \times( \nabla \times A)=\mu_0 \nabla P  $$

We then specify this equation in axisymmetry case, which is supposed to be applied upon the analysis of Tokamak, a device being developed to produce controlled thermonuclear fusion power and and the leading candidate for a practical fusion reactor currently.

<img src="img/Schematic-of-a-tokamak-chamber-and-magnetic-profile.jpg" width=100% alt="hello" title="Tokamak">

The axial symmetry means that, when expressed in the cylindrical coordinate system (R,ϕ,Z), the components of B, namely $B_R, B_z$, and $B_ϕ$, are all independent of ϕ. For this case, it can be proved that an axisymmetric vector potential A suﬃces for expressing the magnetic ﬁeld, i.e., all the components of the vector potential A can also be taken independent of ϕ

$$
 \nabla \times A \underset{\partial_\phi =0}{=}  \left( -\partial_z A_\phi , \  \partial_z A_r - \partial_r A_z, \ \partial_r (rA_\phi)/r \right) 
$$

since the r and z components of curlA are only affected by the θ component of E and the θ component of curlA is only affected by the r and z components of E then we can decouple the three dimensional cylindrical curl into two, two dimensional curl operators $\bold{curl}_{rz}$ and $curl_{rz}$.

$$\begin{aligned}
curl_{rz}(A_r, A_z) = ∂_zA_r − ∂_rA_z \\
\bold{curl}_{rz} A_\phi =  (−∂_zA_\phi, \partial_r (rA_\phi)/r) \\
\end{aligned}  \tag{1} $$

Because of this we are allowed to then break our original 3-D problem into two 2-D problems. One which is called the Meridian Problem, dealing with only the r and z components and the other is known as the Azimuthal Problem, dealing with the θ component. In tokamak literature, (R,Z) planes (i.e., ϕ = const planes) are called the poloidal planes. Equation (3) indicates that two poloidal components of B, namely $B_R$ and $B_Z$, are determined by a single component of A, namely $A_ϕ$. This motivates us to deﬁne a scalar function $\Phi(r,z)=rA_\phi(r,z)$. The the poloidal magnetic ﬁeld $B=B_r\bold{r}+B_z\bold{z}$, with its components be

$$\begin{aligned}
B_r = -\frac{1}{r} \frac{\partial\Phi}{\partial z} \\
B_z = \frac{1}{r} \frac{\partial\Phi}{\partial r} \\
\end{aligned}  $$

respectively.

$$ \frac{1}{r} \left(\frac{\partial^2\Phi}{\partial r^2}+\frac{\partial^2\Phi}{\partial z^2}\right) - \frac{1}{r^2}\frac{\partial\Phi}{\partial r}=-\mu_0J_\phi=-\mu_0 r^2 \frac{\partial p}{\partial \Phi} -\mu_0 \frac{\partial F}{\partial \Phi}$$


## 1.1 Mixed Form
The Cahn-Hilliard equation is a fourth-order equation, so casting it in a weak form would result in the presence of second-order spatial derivatives, and the problem could not be solved using a standard Lagrange finite element basis. A solution is to rephrase the problem as two coupled second-order equations:

$$\begin{aligned}
\frac{\partial c}{\partial t} - \nabla \cdot M \nabla\mu = 0 \quad {\rm in} \ \Omega, \\
\mu -  \frac{d f}{d c} + \lambda \nabla^{2}c = 0 \quad {\rm in} \ \Omega. \\
\end{aligned}  \tag{2} $$

The unknown fields are now c and μ. The weak (variational) form of the problem reads: find $(c,\mu) \in V \times V$ such that

$$\begin{aligned}
\int_{\Omega} \frac{\partial c}{\partial t} q \, {\rm d} x + \int_{\Omega} M \nabla\mu \cdot \nabla q \, {\rm d} x = 0 \quad \forall \ q \in V, \\
\int_{\Omega} \mu v \, {\rm d} x - \int_{\Omega} \frac{d f}{d c} v \, {\rm d} x - \int_{\Omega} \lambda \nabla c \cdot \nabla v \, {\rm d} x
= 0 \quad \forall \ v \in V. \\
\end{aligned}  \tag{3} $$

## 1.2 Time discretisation
Before being able to solve this problem, the time derivative must be dealt with. Apply the θ-method to the mixed weak form of the equation:
$$\begin{aligned}
\int_{\Omega} \frac{c_{n+1} - c_{n}}{dt} q \, {\rm d} x + \int_{\Omega} M \nabla \mu_{n+\theta} \cdot \nabla q \, {\rm d} x = 0 \quad \forall \ q \in V  \\
\int_{\Omega} \mu_{n+1} v  \, {\rm d} x - \int_{\Omega} \frac{d f_{n+1}}{d c} v  \, {\rm d} x - \int_{\Omega} \lambda \nabla c_{n+1} \cdot \nabla v \, {\rm d} x = 0 \quad \forall \ v \in V \\
\end{aligned}  \tag{4} $$

## References

1. S. Li, H. Jiang, Z. Ren, C. Xu - S. Li et. al. "Optimal Tracking for a Divergent-Type Parabolic PDE System in Current Profile Control" , Abstract and Applied Analysis、Vol.2014
2. https://youjunhu.github.io/
3. M. Maraldi, L. Molari, and D. Grandi, “A unified thermodynamic framework for the modelling of diffusive and displacive phase transitions,” International Journal of Engineering Science, vol. 50, no. 1, pp. 31–45, 2012
