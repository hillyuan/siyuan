
# Finite Element Analysis of the Cahn-Hilliard Equation

The Cahn–Hilliard (CH) equation is a mathematical model of the process of a phase separation in a binary alloy [1, 2]. Its physical applications have been extended to many areas of scientific fields such as spinodal decomposition [1, 3], diblock copolymer [4–6], image inpainting [7], multiphase fluid flows [8–10], microstructures with elastic inhomogeneity [11, 12], tumor growth simulation [13, 14], and topology optimization [15, 16]. In this paper, we review and describe the basic mechanism of each model. The basic building-block equation of the mathematical modeling and dynamical model is the CH equation:

## Formulation

An order parameter  c  is defined as the concentration of B atom. The unit of  c  is defined as atomic fraction in this code. The total Gibbs free energy of the system is defined by
$$ \int_\Omega\Bigl(g_{chem}(c)+g_{grad}(\nabla c)\Bigr)d\Omega  $$
where  $g_{chem}$  and  $g_{grad}$  are the chemical free energy and the gradient energy densities, respectively. The gradient energy density is generally expressed by
$$ g_{grad}=\frac{a_c}{2}|\nabla c|^2 $$
where $a_c$ is the gradient energy coefficient.

The time evolution of the order parameter c is given by assuming that the total free energy of the system  G  decreases monotonically with time. For the conserved order parameter, the time evolution equation is derived from the Cahn-Hilliard equation given as:
$$ \frac{\partial c}{\partial t}=\nabla \cdot \Biggl(M_c\nabla\frac{\delta G}{\delta c}\Biggr)=\nabla \cdot(M_c\nabla \mu) $$

where  M is diffusion mobility, μ is the diffusion potential of B atom. According to the total Gibbs free energy,  μ  is expressed by:

$$ \mu=\frac{\delta G}{\delta c} = \frac{\delta g}{\delta c} + \nabla \cdot \frac{\delta g}{\delta (\nabla c)}=\frac{\delta g}{\delta c} +a_c \nabla^2c $$

The Cahn-Hilliard equation is a parabolic equation and is typically used to model phase separation in binary mixtures. It involves first-order time derivatives, and second- and fourth-order spatial derivatives. The equation reads:

$$\begin{aligned}
\frac{\partial c}{\partial t} - \nabla \cdot M \left(\nabla\left(\frac{d f}{d c} - \lambda \nabla^{2}c\right)\right) = 0 \quad {\rm in} \ \Omega, \\
\left(\nabla\left(\frac{d f}{d c} - \lambda \nabla^{2}c\right)\right) = 0 \quad {\rm on} \ \partial\Omega, \\
\lambda \nabla c \cdot n = 0 \quad {\rm on} \ \partial\Omega. \\
\end{aligned}  \tag{1} $$

where c is the unknown field, the function f is usually non-convex in c (a fourth-order polynomial is commonly used), n is the outward directed boundary normal, and M is a scalar parameter.

## 1.1 Mixed Form
The Cahn-Hilliard equation is a fourth-order equation, so casting it in a weak form would result in the presence of second-order spatial derivatives, and the problem could not be solved using a standard Lagrange finite element basis. A solution is to rephrase the problem as two coupled second-order equations:

$$\begin{aligned}
\frac{\partial c}{\partial t} - \nabla \cdot M \nabla\mu = 0 \quad {\rm in} \ \Omega, \\
\mu -  \frac{d f}{d c} + \lambda \nabla^{2}c = 0 \quad {\rm in} \ \Omega. \\
\end{aligned}  \tag{2} $$

The unknown fields are now c and μ. The weak (variational) form of the problem reads: find $(c,\mu) \in V \times V$ such that

$$\begin{aligned}
\int_{\Omega} \frac{\partial c}{\partial t} q \, {\rm d} x + \int_{\Omega} M \nabla\mu \cdot \nabla q \, {\rm d} x = 0 \quad \forall \ q \in V, \\
\int_{\Omega} \mu v \, {\rm d} x - \int_{\Omega} \frac{d f}{d c} v \, {\rm d} x - \int_{\Omega} \lambda \nabla c \cdot \nabla v \, {\rm d} x
= 0 \quad \forall \ v \in V. \\
\end{aligned}  \tag{3} $$

## 1.2 Time discretisation
Before being able to solve this problem, the time derivative must be dealt with. Apply the θ-method to the mixed weak form of the equation:
$$\begin{aligned}
\int_{\Omega} \frac{c_{n+1} - c_{n}}{dt} q \, {\rm d} x + \int_{\Omega} M \nabla \mu_{n+\theta} \cdot \nabla q \, {\rm d} x = 0 \quad \forall \ q \in V  \\
\int_{\Omega} \mu_{n+1} v  \, {\rm d} x - \int_{\Omega} \frac{d f_{n+1}}{d c} v  \, {\rm d} x - \int_{\Omega} \lambda \nabla c_{n+1} \cdot \nabla v \, {\rm d} x = 0 \quad \forall \ v \in V \\
\end{aligned}  \tag{4} $$

## References
http://web.tuat.ac.jp/~yamanaka/pcoms2019/Cahn-Hilliard-2d.html
https://fenicsproject.org/olddocs/dolfin/1.3.0/python/demo/documented/cahn-hilliard/python/documentation.html
1. J. W. Cahn, “On spinodal decomposition,” Acta Metallurgica, vol. 9, no. 9, pp. 795–801, 1961.
2. J. W. Cahn and J. E. Hilliard, "Free Energy of a Non-Uniform System in Interfacial Energy" Journal of Chemical Physics, 28 (1958), pp. 258-267
3. M. Maraldi, L. Molari, and D. Grandi, “A unified thermodynamic framework for the modelling of diffusive and displacive phase transitions,” International Journal of Engineering Science, vol. 50, no. 1, pp. 31–45, 2012
4. R. Choksi, M. A. Peletier, and J. F. Williams, “On the phase diagram for microphase separation of diblock copolymers: an approach via a nonlocal Cahn-Hilliard functional,” SIAM Journal on Applied Mathematics, vol. 69, no. 6, pp. 1712–1738, 2009
5. D. Jeong, S. Lee, Y. Choi, and J. Kim, “Energy-minimizing wavelengths of equilibrium states for diblock copolymers in the hex-cylinder phase,” Current Applied Physics, vol. 15, no. 7, pp.799–804, 2015
6. D. Jeong, J. Shin, Y. Li et al., “Numerical analysis of energyminimizing wavelengths of equilibrium states for diblock copolymers,” Current Applied Physics, vol. 14, no. 9, pp. 1263–1272, 2014.
7. A. L. Bertozzi, S. Esedoglu, and A. Gillette, “Inpainting of binary images using the Cahn-Hilliard equation,” IEEE Transactions on Image Processing, vol. 16, no. 1, pp. 285–291, 2007.
8. V. E. Badalassi, H. D. Ceniceros, and S. Banerjee, “Computation of multiphase systems with phase field models,” Journal of Computational Physics, vol. 190, no. 2, pp. 371–397, 2003.
9. M. Heida, “On the derivation of thermodynamically  consistent boundary conditions for the Cahn–Hilliard–Navier–Stokes system,” International Journal of Engineering Science, vol. 62, pp.126–156, 2013.
10. M. Kotschote and R. Zacher, “Strong solutions in the dynamical theory of compressible fluid mixtures,” Mathematical Models and Methods in Applied Sciences, vol. 25, no. 7, pp. 1217–1256,2015.
11. M. Asle Zaeem, H. El Kadiri, M. F. Horstemeyer, M. Khafizov, and Z. Utegulov, “Effects of internal stresses and intermediate phases on the coarsening of coherent precipitates: a phase-field study,” Current Applied Physics, vol. 12, no. 2, pp. 570–580, 2012.
12. J. Zhu, L.-Q. Chen, and J. Shen, “Morphological evolution during phase separation and coarsening with strong inhomogeneous elasticity,” Modelling and Simulation in Materials Science and Engineering, vol. 9, no. 6, pp. 499–511, 2001.
13. D. Hilhorst, J. Kampmann, T. Nguyen, and K. G. Van Der Zee, “Formal asymptotic limit of a diffuse-interface tumor-growth model,” Mathematical Models and Methods in Applied Sciences, vol. 25, no. 6, pp. 1011–1043, 2015.
14. S. M. Wise, J. S. Lowengrub, H. B. Frieboes, and V. Cristini, “Three-dimensional multispecies nonlinear tumor growth? I: model and numerical method,” Journal of Theoretical Biology, vol. 253, no. 3, pp. 524–543, 2008.
15. M. H. Farshbaf-Shaker and C. Heinemann, “A phase field approach for optimal boundary control of damage processes in two-dimensional viscoelastic media,” Mathematical Models and Methods in Applied Sciences, vol. 25, no. 14, pp. 2749–2793, 2015.
16. S. Zhou and M. Y. Wang, “Multimaterial structural topology optimization with a generalized Cahn–Hilliard model of multiphase transition,” Structural and Multidisciplinary Optimization, vol. 33, no. 2, pp. 89–111, 2007.