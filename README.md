<img src="doc/img/siyuan.png" width=30% alt="hello" title="Title">

# 四元(SiYuan)

Si Yuan Shu (Simplified Chinese: 四元术; traditional Chinese: 四元術) is a Chinese system of algebra for solving polynomial equations with four unkonws created in the 13th century. It is recorded in Zhu Shijie's book, 四元玉鑑（Jade Mirror of the Four Unknowns）, written in 1303, which is awarded as one of the highest level of achievement in ancient Chinese mathematical research. SiYuan, on the other hand, is a Multiphysics PDEs solver. 

四元術は中国元初期の数学者朱世傑が天元術はという中国で生まれた代数問題の解法から発展させた4元の高次連立方程式の解法である。朱世傑の著作『算学啓蒙』（1299年）は日本にも1658年（万治元年）に出版され、これを通して天元術は和算の発展の元となった。四元術は彼の1303年出版された著作『四元玉鑑』に記載されている。その言葉を流用した本ソフトウェアSiYuan(四元)はマルチフィジックスPDEsソルバーである。

“四元术”是元代数学家朱世杰（1249年－1314年）从“天元术”发展出来的四元高次多项式方程求解方法。他的著作《四元玉鉴》代表了中国古代数学的最高成就。本软件是通用多物理场偏微分方程求解器，取名《四元》以致敬中国古代数学家的成就。


# Features

<p align = "center">
<img src = "doc/img/SW.png"  width=50% > 
</p>
<p align = "center">
Dam break (Shallow water Equation)
</p>

<p align = "center">
<img src = "doc/img/MagStatic.png"  width=50% > 
</p>
<p align = "center">
Magnetic Force Line near a magnet (MagnetoStatic Equation)
</p>

<p align = "center">
<img src = "doc/img/CH.png"  width=50% > 
</p>
<p align = "center">
Concentration distribution (Cahn-Hilliard Equation)
</p>

<p align = "center">
<img src = "doc/img/acoustics.png"  width=50% > 
</p>
<p align = "center">
Acoustic pressure distribution (Helmholtz Equation)
</p>

<p align = "center">
<img src = "doc/img/temperature.png"  width=50% > 
</p>
<p align = "center">
Temperature distribution (Convection–diffusion Equation)
</p>

<p align = "center">
<img src = "doc/img/tokamak.png"  width=50% > 
</p>
<p align = "center">
Magnetic flux distribution (Grad–Shafranov Equation)
</p>


# Software architecture
SiYuan heavily leverages the Trilinos and Tianxin Framework, available at:

https://github.com/trilinos/Trilinos.git \
https://github.com/hillyuan/Tianxin.git

SiYuan supports large scalar supercomputing by using MPI, as well as [Kokkos](https://github.com/kokkos/kokkos) hardware abstraction package to support generic manycore computing across a variety of platforms - MPI + [threads, OpenMP, Cuda etc].


# Installation

- Compile and Install Trilinos with Tianxin. If you wish to compile Trilinos with MPI and OpenMP by gcc, the configure file would like:

```cmake: configure.sh
cmake .. \
  -DCMAKE_INSTALL_PREFIX:FILEPATH="where you wat to install" 
  -DCMAKE_CXX_COMPILER=mpic++ \
  -DCMAKE_C_COMPILER=mpicc \
  -DTPL_ENABLE_MPI:BOOL=ON \
  -DKokkos_ENABLE_OPENMP:BOOL=ON \
  -DTrilinos_ENABLE_OpenMP:BOOL=ON  \
  -DTPL_ENABLE_HDF5=ON  \
  -DTPL_ENABLE_Matio=OFF  \
  -DTrilinos_ENABLE_Fortran=ON \
  -DTrilinos_ENABLE_SEACAS=ON \
  -DTrilinos_ENABLE_Amesos2=ON \
  -DTrilinos_ENABLE_Panzer=ON  \
  -DTrilinos_ENABLE_STK=ON  \
  -DTrilinos_ENABLE_EXPLICIT_INSTANTIATION:BOOL=ON
```
Pls see relevent instructuion of Trilinos and [Tianxin](https://github.com/hillyuan/Tianxin) for detail.


- Configure SiYuan by cmake. SiYuan would be compiled using the same compile option of Trilinos. The only thing we must do is to indicate  where your Trilinos is installed. Like "cmake -DTRILINOS_DIR='folder where your Trilinos installed'"
- make it.

# Usage

Just type in "suyuan --i='your input filename'" or "mpirun -np X suyuan --i='your input filename'" when do mpi parallel calculation.


# License
SiYuan is distributed under the terms of the BSD-2 license. All new contributions must be made under this license. See LICENSE and NOTICE for details.

# Contributing
In the spirit of free software, everyone is encouraged to help improve this project. If you discover errors or omissions in the source code, documentation, or website content, please don’t hesitate to submit an issue or open a pull request with a fix. New contributors are always welcome!

