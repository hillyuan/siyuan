configure_file(${CMAKE_CURRENT_SOURCE_DIR}/beam.exo
               ${CMAKE_CURRENT_BINARY_DIR}/beam.exo COPYONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/beam.pg
               ${CMAKE_CURRENT_BINARY_DIR}/beam.pg COPYONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/magnet.gen
               ${CMAKE_CURRENT_BINARY_DIR}/magnet.gen COPYONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/beamgen.gen
               ${CMAKE_CURRENT_BINARY_DIR}/beamgen.gen COPYONLY)

